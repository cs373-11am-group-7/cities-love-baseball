import unittest
import sys

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

PATH = "chromedriver_linux"
URL = "https://develop.citieslovebaseball.me"

# Tests are modeled after Texas Votes
# https://gitlab.com/forbesye/fitsbits/-/tree/master/front-end/gui_tests

class TestSplash(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--window-size=1280,800')
        chrome_options.add_argument('--allow-insecure-localhost')
        chrome_options.add_argument('--start-maximized')

        cls.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
        cls.driver.get("https://develop.citieslovebaseball.me/players/view/456713")
        cls.driver.maximize_window()

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


    def testBushCity(self):
        self.driver.get("https://develop.citieslovebaseball.me/players/view/456713") # player page for Matt Bush
        self.driver.implicitly_wait(3)
        self.driver.find_element(By.LINK_TEXT, 'Arlington').click()
        element = self.driver.find_element(By.XPATH, "//*[text()='VA']")
        assert element.text == 'State: VA'

    def testBushTeam(self):
        self.driver.get("https://develop.citieslovebaseball.me/players/view/456713")
        self.driver.implicitly_wait(3)
        self.driver.find_element(By.LINK_TEXT, 'Texas Rangers').click()
        element = self.driver.find_element(By.XPATH, "//*[text()='TEX']")
        assert element.text == 'Abbreviation: TEX'
        

if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])