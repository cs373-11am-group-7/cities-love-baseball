import unittest
import sys

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

PATH = "chromedriver_linux"
URL = "https://develop.citieslovebaseball.me"

# Tests are modeled after Texas Votes
# https://gitlab.com/forbesye/fitsbits/-/tree/master/front-end/gui_tests


class TestHomePage(unittest.TestCase):
    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--window-size=1280,800')
        chrome_options.add_argument('--allow-insecure-localhost')
        chrome_options.add_argument('--start-maximized')

        cls.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
        cls.driver.get(URL)
        cls.driver.maximize_window()


    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


    def testSearch(self):
        self.driver.get(URL)
        self.driver.find_element(By.CLASS_NAME, 'form-control').send_keys('madison')
        self.driver.find_element(By.LINK_TEXT, 'Search').click()
        self.driver.implicitly_wait(5)
        element = self.driver.find_element(By.CLASS_NAME, 'model-grid-highlight')
        assert element.text == 'Madison'

        
    def testFilter(self):
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Players').click()
        self.driver.find_element(By.CLASS_NAME, 'searchBox').click()
        self.driver.find_elements(By.CLASS_NAME, 'checkbox')[5].click()
        self.driver.implicitly_wait(5)
        element = self.driver.find_element(By.CSS_SELECTOR, 'tr.model-table-row')
        text = element.text.split()
        assert text[1] == 'Wade'


    def testSort(self):
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Players').click()
        self.driver.find_elements(By.CLASS_NAME, 'model-table-sorting')[1].click()
        self.driver.implicitly_wait(5)
        element = self.driver.find_element(By.CSS_SELECTOR, 'tr.model-table-row')
        text = element.text.split()
        assert text[3] == 'Arizona'

        
if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])