import unittest
import sys

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

PATH = "chromedriver_linux"
URL = "https://develop.citieslovebaseball.me"

# Tests are modeled after Texas Votes
# https://gitlab.com/forbesye/fitsbits/-/tree/master/front-end/gui_tests


class TestHomePage(unittest.TestCase):
    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--window-size=1280,800')
        chrome_options.add_argument('--allow-insecure-localhost')
        chrome_options.add_argument('--start-maximized')

        cls.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
        cls.driver.get(URL)
        cls.driver.maximize_window()


    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


    def testBrand(self):
        self.driver.get(URL)
        self.driver.find_element(By.CLASS_NAME, "navbar-brand").click()
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == 'Cities Love Baseball'

        
    def testHome(self):
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Home').click()
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == 'Cities Love Baseball'


    def testAbout(self):
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'About').click()
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == 'About Cities Love Baseball'


    def testCities(self):
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Cities').click()
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == 'Cities'


    def testTeams(self):
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Teams').click()
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == 'Teams'


    def testPlayers(self):
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Players').click()
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == 'Players'

        
if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])