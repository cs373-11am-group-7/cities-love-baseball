import os
from sys import platform

if __name__ == "__main__":
    # Use chromedriver based on OS
    if platform == "win32":
        PATH = "gui_tests/chromedriver_windows.exe"
    elif platform == "linux":
        PATH = "gui_tests/chromedriver_linux"
    elif platform == "darwin":
        PATH = "gui_tests/chromedriver_mac"
    else:
        print("Unsupported OS")
        exit(-1)

    # Run all of the gui tests
    os.system("python3 ./frontend/gui_tests/splashTests.py "           + PATH)
    os.system("python3 ./frontend/gui_tests/navBarTests.py "           + PATH)
    os.system("python3 ./frontend/gui_tests/playerTests.py "           + PATH)
    os.system("python3 ./frontend/gui_tests/searchFilterSortTests.py " + PATH)
