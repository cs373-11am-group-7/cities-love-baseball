export default interface IMusicArtist {
    followers: number;
    popularity: number;
}
