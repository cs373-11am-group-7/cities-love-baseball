import { IModelLink } from "../interfaces/IModelLink";

export interface ITeam {
    id: number;
    logo_url: string;
    facebook_url: string;
    twitter_url: string;
    instagram_url: string;
    youtube_url: string;
    name: string;
    short_name: string;
    abbreviation: string;
    city: IModelLink;
    players: IModelLink[];
    games_played: number;
    runs: number;
    home_runs: number;
    hits: number;
    stolen_bases: number;
    wins: number;
}

export default ITeam;
