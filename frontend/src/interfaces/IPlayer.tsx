import { IModelLink } from "../interfaces/IModelLink";

export interface IPlayer {
    id: number;
    image_url: string;
    highlight_url: string;
    name: string;
    city: IModelLink;
    team: IModelLink;
    age: number;
    birth_city: string;
    height: string;
    weight: number;
    position: string;
    batting_side: string;
    pitching_hand: string;
}

export default IPlayer;
