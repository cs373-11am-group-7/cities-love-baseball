import { IModelLink } from "../interfaces/IModelLink";

export interface ICity {
    id: number;
    image_url: string;
    name: string;
    teams: IModelLink[];
    players: IModelLink[];
    average_commute_time: number;
    median_property_value: number;
    average_population_age: number;
    state: string;
    population: number;
    poverty_rate: number;
    employed_population: number;
    largest_industry: string;
    longitude: number;
    latitude: number;
}

export default ICity;
