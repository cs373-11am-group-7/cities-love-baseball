export default interface IMusicCity {
    longitude: number;
    latitude: number;
}
