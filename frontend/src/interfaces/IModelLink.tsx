export interface IModelLink {
    id: number;
    name: string;
}

export default IModelLink;
