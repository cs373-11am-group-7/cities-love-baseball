import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import { render, screen } from "@testing-library/react";
import App from "./App";

import SplashPage from "./pages/SplashPage";
import Card from "./components/Card";
import NavBar from "./components/NavBar";
import AboutPage from "./pages/AboutPage";
import CityListPage from "./pages/CityListPage";
import PlayerListPage from "./pages/PlayerListPage";
import TeamListPage from "./pages/TeamListPage";
import GridPagination from "./components/GridPagination";
import TablePagination from "./components/TablePagination";

configure({ adapter: new Adapter() });

// Tests are modeled after Texas Votes
// https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/__tests__/main.tests.js

test("SplashTest", () => {
    const splashTest = shallow(<SplashPage />);
    expect(splashTest).toMatchSnapshot();
});

test("CardTest", () => {
    const thisCard = shallow(<Card title="testing" text="howdy" />);
    expect(thisCard).toMatchSnapshot();
});

test("Card clickCard() Test", () => {
    const thisCard = shallow(<Card title="testing" text="howdy" />);
    expect(thisCard.instance().clickCard()).toMatchSnapshot();
});

test("NavBarTest", () => {
    const thisNav = shallow(<NavBar />);
    expect(thisNav).toMatchSnapshot();
});

test("About", () => {
    const aboutTest = shallow(<AboutPage />);
    expect(aboutTest).toMatchSnapshot();
});

test("CityList", () => {
    const cityList = shallow(<CityListPage />);
    expect(cityList).toMatchSnapshot();
});

test("GridPagination Function Test", () => {
    const mockedParams = {
        getItemData: () => "",
        pageSize: 9,
        backendLink: "",
        backendData: "",
        sortBy: "",
        sortDir: 0,
        filterBy: "",
        filterValues: "",
    };
    const cityList = shallow(<GridPagination {...mockedParams} />);
    expect(cityList.instance().getItems().items.length).toBe(0);
});

test("PlayerList", () => {
    const playerList = shallow(<PlayerListPage />);
    expect(playerList).toMatchSnapshot();
});

test("TablePagination Function Test", () => {
    const mockedParams = {
        getItemData: () => "",
        getTableHeader: () => "",
        pageSize: 9,
        backendLink: "",
        backendData: "",
        sortBy: "",
        sortDir: 0,
        filterBy: "",
        filterValues: "",
    };
    const playerList = shallow(<TablePagination {...mockedParams} />);
    expect(playerList.instance().getItems().items.length).toBe(0);
});

test("TeamList", () => {
    const teamList = shallow(<TeamListPage />);
    expect(teamList).toMatchSnapshot();
});

test("Filter Test", () => {
    const mockedParams = {
        getItemData: () => "",
        getTableHeader: () => "",
        pageSize: 10,
        backendLink: "",
        backendData: "",
        sortBy: "",
        sortDir: 0,
        filterBy:
            "name|state|population|average_population_age|median_property_value",
        filterValues: "Y-||0-149999||",
    };
    const playerList = shallow(<TablePagination {...mockedParams} />);
    expect(playerList.instance().getItems().items.length).toBe(0);
});

test("Sorting Test", () => {
    const mockedParams = {
        getItemData: () => "",
        getTableHeader: () => "",
        pageSize: 10,
        backendLink: "",
        backendData: "",
        sortBy: "population",
        sortDir: 2,
        filterBy:
            "name|state|population|average_population_age|median_property_value",
        filterValues: "Y-||0-149999||",
    };
    const playerList = shallow(<TablePagination {...mockedParams} />);
    expect(playerList.instance().getItems().items.length).toBe(0);
});

test("Search Test", () => {
    const mockedParams = {
        getItemData: () => "",
        getTableHeader: () => "",
        pageSize: 10,
        backendLink: "",
        backendData: "",
        sortBy: "",
        sortDir: 0,
        filterBy: "",
        filterValues: "",
        searchFor: "Auburn",
    };
    const playerList = shallow(<TablePagination {...mockedParams} />);
    expect(playerList.instance().getItems().items.length).toBe(0);
});
