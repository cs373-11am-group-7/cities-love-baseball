import React from "react";
import "./CityInstancePage.css";
import ListGroup from "react-bootstrap/ListGroup";
import { Card, ICardProps } from "../components/Card";
import { RouteComponentProps } from "react-router";
import ICity from "../interfaces/ICity";
import { backendApiUrl } from "../Constants";

interface IParams {
    id: string;
}

interface IProps extends RouteComponentProps<IParams> {}

interface IState {
    city?: ICity;
}

class CityInstancePage extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            city: undefined,
        };
    }

    async fetchCity(): Promise<ICity> {
        let id = parseInt(this.props.match.params.id);
        let response = await fetch(backendApiUrl + "/city/id=" + id);
        let data = await response.json();

        return data;
    }

    async componentDidMount() {
        let city = await this.fetchCity();

        this.setState({
            city: city,
        });
    }

    render() {
        let city = this.state.city;

        if (!city) {
            return <div></div>;
        }

        const cityCard: ICardProps = {
            width: 35,
            image: city.image_url,
            imageFull: false,
            title: city.name,
        };

        const teamsCard: ICardProps = {
            width: 35,
            title: "Teams",
        };

        const playersCard: ICardProps = {
            width: 35,
            title: "Players",
        };

        const mapCard: ICardProps = {
            width: 35,
            title: "Map",
        };

        const charityCard: ICardProps = {
            width: 35,
            title: "Charities Promoting Baseball",
        };

        const commute_time =
            city.average_commute_time == null
                ? "No Data"
                : city.average_commute_time.toLocaleString();
        const property_value =
            city.median_property_value == null
                ? "No Data"
                : city.median_property_value.toLocaleString();
        const population_age =
            city.average_population_age == null
                ? "No Data"
                : city.average_population_age.toLocaleString();
        const population =
            city.population == null
                ? "No Data"
                : city.population.toLocaleString();
        const poverty_rate =
            city.poverty_rate == null
                ? "No Data"
                : (city.poverty_rate * 100).toLocaleString();
        const employed_population =
            city.employed_population == null
                ? "No Data"
                : city.employed_population.toLocaleString();
        const largest_industry =
            city.largest_industry == null ? "No Data" : city.largest_industry;

        return (
            <div>
                <br />
                <br />
                <br />
                <div className="city-card">
                    <Card {...cityCard}>
                        <ListGroup className="list-group-flush">
                            <ListGroup.Item>State: {city.state}</ListGroup.Item>
                            <ListGroup.Item>
                                Average Commute Time: {commute_time}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Median Property Value: {property_value}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Average Population Age: {population_age}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Population: {population}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Poverty Rate: {poverty_rate}%
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Employed Population: {employed_population}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Largest Industry: {largest_industry}
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                </div>
                <br />
                <br />
                <div className="city-card">
                    <Card {...teamsCard}>
                        <ListGroup className="list-group-flush">
                            {city.teams.map((team) => (
                                <ListGroup.Item>
                                    <a href={"/teams/view/" + team.id}>
                                        {team.name}
                                    </a>
                                </ListGroup.Item>
                            ))}
                            {city.teams.length === 0 && (
                                <ListGroup.Item>
                                    {city.name} does not have any baseball teams
                                </ListGroup.Item>
                            )}
                        </ListGroup>
                    </Card>
                </div>
                <br />
                <br />
                <div className="city-card">
                    <Card {...playersCard}>
                        <ListGroup className="list-group-flush">
                            {city.players.map((player) => (
                                <ListGroup.Item>
                                    <a href={"/players/view/" + player.id}>
                                        {player.name}
                                    </a>
                                </ListGroup.Item>
                            ))}
                            {city.players.length === 0 && (
                                <ListGroup.Item>
                                    {city.name} does not have any baseball
                                    players
                                </ListGroup.Item>
                            )}
                        </ListGroup>
                    </Card>
                </div>
                <br />
                <br />
                <div className="city-card">
                    <Card {...mapCard}>
                        <div className="city-map-container">
                            <iframe
                                title="Map"
                                className="city-map"
                                src={
                                    "https://www.google.com/maps/embed/v1/place?key=AIzaSyCu8P9RtsiNu5eShOa5OPIfWXvSZiddvJE&q=" +
                                    city.name +
                                    ", " +
                                    city.state
                                }
                            ></iframe>
                        </div>
                    </Card>
                </div>
                <br />
                <br />
                <div className="city-card">
                    <Card {...charityCard}>
                        <a href="http://www.turn2foryouth.com/new-page">
                            Turn Two for Youth
                        </a>
                        <br />
                        <a href="https://pifbs.org/">
                            Pitch In For Baseball & Softball
                        </a>
                    </Card>
                </div>
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default CityInstancePage;
