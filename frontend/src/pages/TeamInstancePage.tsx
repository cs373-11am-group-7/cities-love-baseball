import React from "react";
import "./TeamInstancePage.css";
import ListGroup from "react-bootstrap/ListGroup";
import { Card, ICardProps } from "../components/Card";
import { RouteComponentProps } from "react-router";
import { Timeline } from "react-twitter-widgets";
import ITeam from "../interfaces/ITeam";
import { backendApiUrl } from "../Constants";

interface IParams {
    id: string;
}

interface IProps extends RouteComponentProps<IParams> {}

interface IState {
    team?: ITeam;
}

class TeamInstancePage extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            team: undefined,
        };
    }

    async fetchTeam(): Promise<ITeam> {
        let id = parseInt(this.props.match.params.id);
        let response = await fetch(backendApiUrl + "/team/id=" + id);
        let data = await response.json();

        return data;
    }

    async componentDidMount() {
        let team = await this.fetchTeam();

        this.setState({
            team: team,
        });
    }

    render() {
        let team = this.state.team;

        if (!team) {
            return <div></div>;
        }

        const teamCard: ICardProps = {
            width: 35,
            image: team.logo_url,
            imageFull: true,
            title: team.name,
        };

        const playersCard: ICardProps = {
            width: 35,
            title: "Players",
        };

        const twitterCard: ICardProps = {
            width: 35,
        };

        const twitterProfile = team.twitter_url.replace("twitter.com/", "");

        return (
            <div>
                <br />
                <br />
                <br />
                <div className="team-card">
                    <Card {...teamCard}>
                        <ListGroup className="list-group-flush">
                            <ListGroup.Item>
                                Short Name: {team.short_name}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Abbreviation: {team.abbreviation}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                City:{" "}
                                <a href={"/cities/view/" + team.city.id}>
                                    {team.city.name}
                                </a>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Games: {team.games_played.toLocaleString()}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Wins: {team.wins.toLocaleString()}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Runs: {team.runs.toLocaleString()}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Home Runs: {team.home_runs.toLocaleString()}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Hits: {team.hits.toLocaleString()}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Stolen Bases:{" "}
                                {team.stolen_bases.toLocaleString()}
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                </div>
                <br />
                <br />
                <div className="team-card">
                    <Card {...playersCard}>
                        <ListGroup className="list-group-flush">
                            {team.players.map((player) => (
                                <ListGroup.Item>
                                    <a href={"/players/view/" + player.id}>
                                        {player.name}
                                    </a>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    </Card>
                </div>
                <br />
                <br />
                <div className="team-card">
                    <Card {...twitterCard}>
                        <Timeline
                            dataSource={{
                                sourceType: "profile",
                                screenName: twitterProfile,
                            }}
                            options={{ height: "800" }}
                        />
                    </Card>
                </div>
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default TeamInstancePage;
