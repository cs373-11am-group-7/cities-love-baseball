import React from "react";
import "./PlayerListPage.css";
import ModelTable from "../components/ModelTable";
import {
    heightFilter,
    numberFilter,
    stringFilter,
} from "../components/FilterBar";
import SearchBar from "../components/SearchBar";

const teams = [
    "Arizona Diamondbacks",
    "Atlanta Braves",
    "Baltimore Orioles",
    "Boston Red Sox",
    "Chicago Cubs",
    "Chicago White Sox",
    "Cincinnati Reds",
    "Cleveland Indians",
    "Colorado Rockies",
    "Detroit Tigers",
    "Houston Astros",
    "Kansas City Royals",
    "Los Angeles Angels",
    "Los Angeles Dodgers",
    "Miami Marlins",
    "Milwaukee Brewers",
    "Minnesota Twins",
    "New York Mets",
    "New York Yankees",
    "Oakland Athletics",
    "Philadelphia Phillies",
    "Pittsburgh Pirates",
    "San Diego Padres",
    "San Francisco Giants",
    "Seattle Mariners",
    "St. Louis Cardinals",
    "Tampa Bay Rays",
    "Texas Rangers",
    "Toronto Blue Jays",
    "Washington Nationals",
];

class PlayerListPage extends React.Component {
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="player-list-title">Players</h1>
                <br />
                <br />
                <SearchBar modelLink={"players"} />
                <br />
                <ModelTable
                    modelLink="players"
                    backendLink="player"
                    backendData="players"
                    fields={[
                        {
                            name: "name",
                            display: "Name",
                        },
                        {
                            name: "team.name",
                            display: "Team",
                        },
                        {
                            name: "age",
                            display: "Age",
                        },
                        {
                            name: "weight",
                            display: "Weight",
                        },
                        {
                            name: "height",
                            display: "Height",
                        },
                    ]}
                    filters={[
                        {
                            name: "name",
                            display: "Name",
                            values: stringFilter(),
                        },
                        {
                            name: "team.name",
                            display: "Team",
                            values: teams.map((team) => {
                                return {
                                    display: team,
                                    value: team,
                                };
                            }),
                        },
                        {
                            name: "age",
                            display: "Age",
                            values: numberFilter(20, 40, 5),
                        },
                        {
                            name: "weight",
                            display: "Weight",
                            values: numberFilter(140, 280, 20),
                        },
                        {
                            name: "height",
                            display: "Height",
                            values: heightFilter(66, 81, 3),
                        },
                    ]}
                />
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default PlayerListPage;
