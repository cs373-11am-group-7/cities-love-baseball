import React from "react";
import { Card, ICardProps } from "../components/Card";
import ListGroup from "react-bootstrap/ListGroup";
import { RouteComponentProps } from "react-router";
import "./PlayerInstancePage.css";
import IPlayer from "../interfaces/IPlayer";
import { backendApiUrl } from "../Constants";

interface IParams {
    id: string;
}

interface IProps extends RouteComponentProps<IParams> {}

interface IState {
    player?: IPlayer;
}

const positionTooltips: any = {
    Outfield: "Positioned in the outer edges of the field called the outfield.",
    Outfielder:
        "Positioned in the outer edges of the field called the outfield.",
    "First Base": "Positioned next to the first base.",
    Catcher:
        "Positioned behind the hitter and catches the ball thrown by the pitcher.",
    "Second Base": "Positioned between the first and second base.",
    "Designated Hitter": "Hits instead of the pitcher.",
    Shortstop: "Positioned between the second and third base.",
    "Third Base": "Positioned next to the third base.",
    Pitcher:
        "Positioned in the center of the baseball diamond and throws the ball to the hitter.",
};

class PlayerInstancePage extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            player: undefined,
        };
    }

    async fetchPlayer(): Promise<IPlayer> {
        let id = parseInt(this.props.match.params.id);
        let response = await fetch(backendApiUrl + "/player/id=" + id);
        let data = await response.json();

        return data;
    }

    async componentDidMount() {
        let player = await this.fetchPlayer();

        this.setState({
            player: player,
        });
    }

    render() {
        let player = this.state.player;

        if (!player) {
            return <div></div>;
        }

        const playerCard: ICardProps = {
            width: 35,
            image: player.image_url,
            imageFull: false,
            title: player.name,
        };

        const highlightCard: ICardProps = {
            width: 35,
            title: "Highlight",
        };

        return (
            <div>
                <br />
                <br />
                <br />
                <div className="player-card">
                    <Card {...playerCard}>
                        <ListGroup className="list-group-flush">
                            <ListGroup.Item>
                                City:{" "}
                                <a href={"/cities/view/" + player.city.id}>
                                    {player.city.name}
                                </a>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                MLB Team:{" "}
                                <a href={"/teams/view/" + player.team.id}>
                                    {player.team.name}
                                </a>
                            </ListGroup.Item>
                            <ListGroup.Item>Age: {player.age}</ListGroup.Item>
                            <ListGroup.Item>
                                Birth City: {player.birth_city}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Height: {player.height}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Weight: {player.weight}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Position:&nbsp;
                                <div className="player-position">
                                    {player.position}
                                    <span className="player-position-tooltip">
                                        {positionTooltips[player.position]}
                                    </span>
                                </div>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Batting Hand: {player.batting_side}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                Throwing Hand: {player.pitching_hand}
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                </div>
                <br />
                <br />
                <div className="player-card">
                    <Card {...highlightCard}>
                        {player.highlight_url !== null && (
                            <video className="player-highlight" controls>
                                <source
                                    src={player.highlight_url}
                                    type="video/mp4"
                                />
                                Embedded videos are not supported by your
                                browser
                            </video>
                        )}
                        {player.highlight_url === null && (
                            <div>
                                {player.name} does not have any highlights
                            </div>
                        )}
                    </Card>
                </div>
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default PlayerInstancePage;
