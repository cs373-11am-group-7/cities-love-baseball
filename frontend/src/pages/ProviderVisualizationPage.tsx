import React from "react";
import "./ProviderVisualizationPage.css";
import * as d3 from "d3";
import { providerApiUrl } from "../Constants";
import IMusicCity from "../interfaces/IMusicCity";
import IMusicArtist from "../interfaces/IMusicArtist";
import world from "../data/world.json";
import LoadingCircle from "../components/LoadingCircle";

interface IProps {}

interface IState {
    loading: boolean;
}

export default class ProviderVisualizationPage extends React.Component<
    IProps,
    IState
> {
    private readonly v1Ref = React.createRef<SVGSVGElement>();
    private readonly v2Ref = React.createRef<SVGSVGElement>();
    private readonly v3Ref = React.createRef<SVGSVGElement>();

    constructor(props: IProps) {
        super(props);

        this.state = {
            loading: true,
        };
    }

    async fetchCities(): Promise<IMusicCity[]> {
        let cityResponse = await fetch(
            providerApiUrl + "/cities?page=1&step=10000"
        );
        let data = await cityResponse.json();
        let cities = data.results as IMusicCity[];

        return cities;
    }

    async fetchArtists(): Promise<IMusicArtist[]> {
        let artistResponse = await fetch(
            providerApiUrl + "/artists?step=10000&page=1"
        );
        let data = await artistResponse.json();
        let artists = data.results as IMusicArtist[];

        return artists;
    }

    renderVisualizationOne(artists: IMusicArtist[]) {
        const width = 1000;
        const height = 600;

        const marginTop = 20;
        const marginRight = 30;
        const marginBottom = 30;
        const marginLeft = 40;

        const radius = 3;

        const inset = radius * 2;

        const xRange = [marginLeft + inset, width - marginRight - inset];
        const yRange = [height - marginBottom - inset, marginTop + inset];

        const color = "steelBlue";

        const X = d3.map(artists, (d: IMusicArtist) => d.followers);
        const I = d3.range(X.length);

        const bins = d3
            .bin()
            .thresholds(20)
            .value((i) => X[i])(I);

        const xDomain = [
            bins[0].x0 as number,
            bins[bins.length - 1].x1 as number,
        ];
        const yDomain = [0, d3.max(bins, (I) => I.length) as number];

        const xScale = d3.scaleLinear(xDomain, xRange);
        const yScale = d3.scaleLinear(yDomain, yRange);
        const xAxis = d3.axisBottom(xScale).ticks(width / 80);
        const yAxis = d3.axisLeft(yScale).ticks(height / 50);

        const svg = d3
            .select(this.v1Ref.current)
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", `0 0 ${width} ${height}`)
            .attr(
                "style",
                "max-width: 100%; height: auto; height: intrinsic; color: white"
            );

        svg.append("g")
            .attr("transform", `translate(${marginLeft},0)`)
            .call(yAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick link")
                    .clone()
                    .attr("x2", width - marginLeft - marginRight)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", -marginLeft)
                    .attr("y", 10)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "start")
                    .text("Frequency")
            );

        svg.append("g")
            .attr("fill", color)
            .selectAll("rect")
            .data(bins)
            .join("rect")
            .attr("x", (d) => xScale(d.x0 as number) + inset)
            .attr("width", (d) =>
                Math.max(
                    0,
                    xScale(d.x1 as number) -
                        xScale(d.x0 as number) -
                        inset -
                        inset
                )
            )
            .attr("y", (d) => yScale(d.length))
            .attr("height", (d) => yScale(0) - yScale(d.length))
            .append("title");

        svg.append("g")
            .attr("transform", `translate(0,${height - marginBottom})`)
            .call(xAxis)
            .call((g) =>
                g
                    .append("text")
                    .attr("x", width - marginRight)
                    .attr("y", 27)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "end")
                    .text("Followers")
            );
    }

    renderVisualizationTwo(artists: IMusicArtist[]) {
        const width = 1000;
        const height = 600;

        const marginTop = 20;
        const marginRight = 30;
        const marginBottom = 30;
        const marginLeft = 40;

        const radius = 3;

        const inset = radius * 2;

        const xRange = [marginLeft + inset, width - marginRight - inset];
        const yRange = [height - marginBottom - inset, marginTop + inset];

        const fill = "none";
        const stroke = "currentColor";
        const strokeWidth = 1.5;

        const X = d3.map(artists, (d: IMusicArtist) => d.followers);
        const Y = d3.map(artists, (d: IMusicArtist) => d.popularity);
        const I = d3
            .range(X.length)
            .filter((i) => !isNaN(X[i]) && !isNaN(Y[i]));

        const xDomain = d3.extent(X) as [number, number];
        const yDomain = d3.extent(Y) as [number, number];

        const xScale = d3.scaleLinear(xDomain, xRange);
        const yScale = d3.scaleLinear(yDomain, yRange);
        const xAxis = d3.axisBottom(xScale).ticks(width / 80);
        const yAxis = d3.axisLeft(yScale).ticks(height / 50);

        const svg = d3
            .select(this.v2Ref.current)
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", `0 0 ${width} ${height}`)
            .attr(
                "style",
                "max-width: 100%; height: auto; height: intrinsic; color: white"
            );

        svg.append("g")
            .attr("transform", `translate(0,${height - marginBottom})`)
            .call(xAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick link")
                    .clone()
                    .attr("y2", marginTop + marginBottom - height)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", width)
                    .attr("y", marginBottom - 4)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "end")
                    .text("Followers")
            );

        svg.append("g")
            .attr("transform", `translate(${marginLeft},0)`)
            .call(yAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick link")
                    .clone()
                    .attr("x2", width - marginLeft - marginRight)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", -marginLeft)
                    .attr("y", 10)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "start")
                    .text("Popularity")
            );

        svg.append("g")
            .attr("fill", fill)
            .attr("stroke", stroke)
            .attr("stroke-width", strokeWidth)
            .selectAll("circle")
            .data(I)
            .join("circle")
            .attr("cx", (i) => xScale(X[i]))
            .attr("cy", (i) => yScale(Y[i]))
            .attr("r", radius);
    }

    async renderVisualizationThree(cities: IMusicCity[]) {
        const width = 2000;
        const height = 1000;

        const radius = 1;

        const fill = "none";
        const stroke = "red";
        const strokeWidth = 2;

        const svg = d3
            .select(this.v3Ref.current)
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", `0 0 ${width} ${height}`)
            .attr(
                "style",
                "max-width: 100%; height: auto; height: intrinsic; color: white"
            );

        const projection = d3
            .geoNaturalEarth1()
            .scale(width / 6)
            .translate([width / 2, height / 2]);

        svg.append("g")
            .selectAll("path")
            .data(world.features)
            .enter()
            .append("path")
            .attr("fill", "#69b3a2")
            .attr("d", d3.geoPath().projection(projection) as any)
            .style("stroke", "#fff");

        svg.append("g")
            .attr("fill", fill)
            .attr("stroke", stroke)
            .attr("stroke-width", strokeWidth)
            .selectAll("circle")
            .data(cities)
            .join("circle")
            .attr(
                "transform",
                (d) =>
                    "translate(" + projection([d.longitude, d.latitude]) + ")"
            )
            .attr("r", radius);
    }

    async componentDidMount() {
        let cities = await this.fetchCities();
        let artists = await this.fetchArtists();

        this.renderVisualizationOne(artists);
        this.renderVisualizationTwo(artists);
        await this.renderVisualizationThree(cities);

        await this.setState({
            loading: false,
        });
    }
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="visualization-title">Provider Visualizations</h1>
                <br />
                <br />
                <div>
                    <h2 className="visualization-title">Artist Followers</h2>
                    <LoadingCircle loading={this.state.loading} />
                    <svg ref={this.v1Ref}></svg>
                </div>
                <br />
                <br />
                <div>
                    <h2 className="visualization-title">
                        Popularity by Followers
                    </h2>
                    <LoadingCircle loading={this.state.loading} />
                    <svg ref={this.v2Ref}></svg>
                </div>
                <br />
                <br />
                <div>
                    <h2 className="visualization-title">Map of Cities</h2>
                    <LoadingCircle loading={this.state.loading} />
                    <svg ref={this.v3Ref}></svg>
                </div>
                <br />
                <br />
            </div>
        );
    }
}
