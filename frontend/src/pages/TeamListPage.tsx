import React from "react";
import "./TeamListPage.css";
import ModelGrid from "../components/ModelGrid";
import { numberFilter, stringFilter } from "../components/FilterBar";
import SearchBar from "../components/SearchBar";

class TeamListPage extends React.Component {
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="team-list-title">Teams</h1>
                <br />
                <br />
                <SearchBar modelLink={"teams"} />
                <br />
                <ModelGrid
                    modelLink="teams"
                    backendLink="team"
                    backendData="teams"
                    fields={[
                        {
                            name: "games_played",
                            display: "Games",
                        },
                        {
                            name: "wins",
                            display: "Wins",
                        },
                        {
                            name: "runs",
                            display: "Runs",
                        },
                        {
                            name: "home_runs",
                            display: "Homeruns",
                        },
                        {
                            name: "hits",
                            display: "Hits",
                        },
                        {
                            name: "stolen_bases",
                            display: "Stolen Bases",
                        },
                    ]}
                    filters={[
                        {
                            name: "name",
                            display: "Name",
                            values: stringFilter(),
                        },
                        {
                            name: "games_played",
                            display: "Games",
                            values: numberFilter(2000, 22000, 4000),
                        },
                        {
                            name: "wins",
                            display: "Wins",
                            values: numberFilter(1000, 11000, 2000),
                        },
                        {
                            name: "runs",
                            display: "Runs",
                            values: numberFilter(15000, 90000, 15000),
                        },
                        {
                            name: "home_runs",
                            display: "Homeruns",
                            values: numberFilter(3000, 15000, 3000),
                        },
                        {
                            name: "hits",
                            display: "Hits",
                            values: numberFilter(30000, 180000, 30000),
                        },
                        {
                            name: "stolen_bases",
                            display: "Stolen Bases",
                            values: numberFilter(2000, 16000, 2000),
                        },
                    ]}
                    sorts={[
                        {
                            display: "None",
                            value: "|",
                        },
                        {
                            display: "Name (Asc.)",
                            value: "name|1",
                        },
                        {
                            display: "Name (Desc.)",
                            value: "name|2",
                        },
                        {
                            display: "Games (Asc.)",
                            value: "games_played|1",
                        },
                        {
                            display: "Games (Desc.)",
                            value: "games_played|2",
                        },
                        {
                            display: "Wins (Asc.)",
                            value: "wins|1",
                        },
                        {
                            display: "Wins (Desc.)",
                            value: "wins|2",
                        },
                        {
                            display: "Runs (Asc.)",
                            value: "runs|1",
                        },
                        {
                            display: "Runs (Desc.)",
                            value: "runs|2",
                        },
                        {
                            display: "Homeruns (Asc.)",
                            value: "home_runs|1",
                        },
                        {
                            display: "Homeruns (Desc.)",
                            value: "home_runs|2",
                        },
                        {
                            display: "Hits (Asc.)",
                            value: "hits|1",
                        },
                        {
                            display: "Hits (Desc.)",
                            value: "hits|2",
                        },
                        {
                            display: "Stolen Bases (Asc.)",
                            value: "stolen_bases|1",
                        },
                        {
                            display: "Stolen Bases (Desc.)",
                            value: "stolen_bases|2",
                        },
                    ]}
                />
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default TeamListPage;
