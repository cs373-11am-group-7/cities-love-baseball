import React from "react";
import "./CityListPage.css";
import ModelTable from "../components/ModelTable";
import { numberFilter, stringFilter } from "../components/FilterBar";
import SearchBar from "../components/SearchBar";

const states = [
    "AK",
    "AL",
    "AR",
    "AZ",
    "CA",
    "CO",
    "CT",
    "DC",
    "DE",
    "FL",
    "GA",
    "HI",
    "IA",
    "ID",
    "IL",
    "IN",
    "KS",
    "KY",
    "LA",
    "MA",
    "MD",
    "ME",
    "MI",
    "MN",
    "MO",
    "MS",
    "MT",
    "NC",
    "ND",
    "NE",
    "NH",
    "NJ",
    "NM",
    "NV",
    "NY",
    "OH",
    "OK",
    "OR",
    "PA",
    "PR",
    "RI",
    "SC",
    "SD",
    "TN",
    "TX",
    "UT",
    "VA",
    "WA",
    "WI",
];

class CityListPage extends React.Component {
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="city-list-title">Cities</h1>
                <br />
                <br />
                <SearchBar modelLink={"cities"} />
                <br />
                <ModelTable
                    modelLink="cities"
                    backendLink="city"
                    backendData="cities"
                    fields={[
                        {
                            name: "name",
                            display: "Name",
                        },
                        {
                            name: "state",
                            display: "State",
                        },
                        {
                            name: "population",
                            display: "Population",
                        },
                        {
                            name: "average_population_age",
                            display: "Average Population Age",
                        },
                        {
                            name: "median_property_value",
                            display: "Median Property Value",
                        },
                    ]}
                    filters={[
                        {
                            name: "name",
                            display: "Name",
                            values: stringFilter(),
                        },
                        {
                            name: "state",
                            display: "State",
                            values: states.map((state) => {
                                return {
                                    display: state,
                                    value: state,
                                };
                            }),
                        },
                        {
                            name: "population",
                            display: "Population",
                            values: numberFilter(0, 1500000, 150000),
                        },
                        {
                            name: "average_population_age",
                            display: "Average Population Age",
                            values: numberFilter(20, 50, 5),
                        },
                        {
                            name: "median_property_value",
                            display: "Median Property Value",
                            values: numberFilter(0, 1000000, 100000),
                        },
                    ]}
                />
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default CityListPage;
