import React from "react";
import "./PlayerListPage.css";
import ModelGrid from "../components/ModelGrid";
import SearchBar from "../components/SearchBar";

class SplashSearchPage extends React.Component {
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="player-list-title">Search</h1>
                <br />
                <SearchBar modelLink={""} />
                <br />
                <h1 className="player-list-title">Teams:</h1>
                <ModelGrid
                    modelLink="teams"
                    backendLink="team"
                    backendData="teams"
                    disableFilter
                    fields={[
                        {
                            name: "short_name",
                            display: "Short Name",
                        },
                        {
                            name: "abbreviation",
                            display: "Abbreviation",
                        },
                        {
                            name: "city.name",
                            display: "City",
                        },
                    ]}
                    filters={[]}
                    sorts={[]}
                />
                <h1 className="player-list-title">Cities:</h1>
                <ModelGrid
                    modelLink="cities"
                    backendLink="city"
                    backendData="cities"
                    disableFilter
                    fields={[
                        {
                            name: "state",
                            display: "State",
                        },
                        {
                            name: "largest_industry",
                            display: "Largest Industry",
                        },
                    ]}
                    filters={[]}
                    sorts={[]}
                />
                <h1 className="player-list-title">Players:</h1>
                <ModelGrid
                    modelLink="players"
                    backendLink="player"
                    backendData="players"
                    disableFilter
                    fields={[
                        {
                            name: "team.name",
                            display: "Team",
                        },
                        {
                            name: "city.name",
                            display: "City",
                        },
                        {
                            name: "birth_city",
                            display: "Birth City",
                        },
                        {
                            name: "position",
                            display: "Position",
                        },
                        {
                            name: "batting_side",
                            display: "Batting Side",
                        },
                        {
                            name: "pitching_hand",
                            display: "Pitching Hand",
                        },
                    ]}
                    filters={[]}
                    sorts={[]}
                />
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default SplashSearchPage;
