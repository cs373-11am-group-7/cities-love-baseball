import React from "react";
import "./AboutPage.css";
import CardGrid from "../components/CardGrid";
import { ICardProps } from "../components/Card";
import { gitlabApiUrl } from "../Constants";
import coletteImage from "../images/colette-image.png";
import matthewImage from "../images/matthew-image.jpg";
import michaelImage from "../images/michael-image.jpg";
import nathanImage from "../images/nathan-image.jpg";
import srikarImage from "../images/srikar-image.jpg";

const repository = {
    link: "https://gitlab.com/cs373-11am-group-7/cities-love-baseball",
    apiLink: "https://documenter.getpostman.com/view/16362325/UUy39mFw",
    id: "29949233",
};

const developers = [
    {
        image: coletteImage,
        name: "Colette Montminy",
        gitlabLink: "https://gitlab.com/cmontminy",
        gitlabName: "cmontminy",
        gitlabCommitName: "Colette Montminy",
        bio: "I am a senior CS student who's interested in data science and machine learning. Outside of school I like to play video games and ride my longboard.",
        role: "Front-End",
        tests: 8,
    },
    {
        image: matthewImage,
        name: "Matthew Whorton",
        gitlabLink: "https://gitlab.com/MatthewWhorton",
        gitlabName: "MatthewWhorton",
        gitlabCommitName: "Matthew Whorton",
        bio: "I am a Senior CS Student who has been programming for most of my life. In my free time I like to play and design video games.",
        role: "Full-Stack",
        tests: 2,
    },
    {
        image: michaelImage,
        name: "Michael Kavas",
        gitlabLink: "https://gitlab.com/MichaelKavas",
        gitlabName: "MichaelKavas",
        gitlabCommitName: "MichaelKavas",
        bio: "I am a Senior from Dallas studying CS, interested in web and game development. I like to play music and exercise in my free time.",
        role: "Backend/Devops",
        tests: 12,
    },
    {
        image: nathanImage,
        name: "Nathan Eisenberg",
        gitlabLink: "https://gitlab.com/nathaneisenberg95",
        gitlabName: "nathaneisenberg95",
        gitlabCommitName: "Nathan Eisenberg",
        bio: "I'm a Senior CS student and I'm interested in artificial intelligence. I'm on UT's wakeboarding club, I play a handful of instruments, and I like video games.",
        role: "Front-End",
        tests: 11,
    },
    {
        image: srikarImage,
        name: "Srikar Ganti",
        gitlabLink: "https://gitlab.com/srikarsganti",
        gitlabName: "srikarsganti",
        gitlabCommitName: "srikarsganti",
        bio: "I am a Junior from Houston studying Computer Science and minoring in History. I like to watch stand-up and go sailing in my free time.",
        role: "Project Manager / Full-Stack",
        tests: 14,
    },
];

const apis = [
    {
        name: "Gitlab API",
        desc: "RESTful API used for gathering information about the repository",
        link: "https://docs.gitlab.com/ee/api/",
        image: "https://docs.gitlab.com/assets/images/gitlab-logo.svg",
    },
    {
        name: "MLB Stats API",
        desc: "RESTful API used for gathering information about baseball",
        link: "https://statsapi.mlb.com/",
        image: "https://statsapi.mlb.com/logo.94f6aeb4.png",
    },
    {
        name: "Data USA API",
        desc: "RESTful API used for gathering information on cities",
        link: "https://datausa.io/about/api/",
        image: "https://datausa.io/images/home/logo-shadow.png",
    },
    {
        name: "The Sports DB API",
        desc: "RESTful API used for gathering information on baseball teams and players",
        link: "https://www.thesportsdb.com/api.php",
        image: "https://www.thesportsdb.com/images/logo32.png",
    },
    {
        name: "Bing Image Search API",
        desc: "RESTful API used for searching for city images",
        link: "https://rapidapi.com/microsoft-azure-org-microsoft-cognitive-services/api/bing-image-search1/",
        image: "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Bing_logo_%282016%29.svg/1280px-Bing_logo_%282016%29.svg.png",
    },
    {
        name: "Open Weather API",
        desc: "RESTful API used for getting city locations",
        link: "https://openweathermap.org/api",
        image: "https://openweathermap.org/themes/openweathermap/assets/img/logo_white_cropped.png",
    },
    {
        name: "Unsplash Image",
        desc: "Website used to get the images for the splash page",
        link: "https://unsplash.com/",
        image: "http://assets.stickpng.com/thumbs/5cb4839d5f1b6d3fbadece7c.png",
    },
    {
        name: "Sports Logos",
        desc: "Website used to find MLB team logos",
        link: "https://www.sportslogos.net/index.php",
        image: "https://www.sportslogos.net/img/design/sprites.png",
    },
    {
        name: "Baseball Reference",
        desc: "Website used to find information on the MLB",
        link: "https://www.baseball-reference.com/",
        image: "https://d2p3bygnnzw9w3.cloudfront.net/req/202112021/logos/br-logo.svg",
    },
];

const tools = [
    {
        name: "React",
        desc: "JavaScript library for building user interfaces",
        link: "https://reactjs.org/",
        image: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0xMS41IC0xMC4yMzE3NCAyMyAyMC40NjM0OCI+CiAgPHRpdGxlPlJlYWN0IExvZ288L3RpdGxlPgogIDxjaXJjbGUgY3g9IjAiIGN5PSIwIiByPSIyLjA1IiBmaWxsPSIjNjFkYWZiIi8+CiAgPGcgc3Ryb2tlPSIjNjFkYWZiIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIi8+CiAgICA8ZWxsaXBzZSByeD0iMTEiIHJ5PSI0LjIiIHRyYW5zZm9ybT0icm90YXRlKDYwKSIvPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIiB0cmFuc2Zvcm09InJvdGF0ZSgxMjApIi8+CiAgPC9nPgo8L3N2Zz4K",
    },
    {
        name: "React Bootstrap",
        desc: "Front-end framework with pre-built components",
        link: "https://react-bootstrap.github.io/",
        image: "data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyMTI3Ljk4IDIwMTYuODciIGZpbGw9IiM0MWUwZmQiPgogIDxwYXRoICBkPSJNODg4IDExMTkuMTdjMTYtMzIuNjIgMzIuODQtNjUuMzcgNTAuMTctOTcuMzVsLjE1LS4yNyAxOC4zOS0zNS42OCAyNy4xNi00NSAuMTctLjI5YzEwLjE0LTE3LjI2IDIwLjY2LTM0LjY4IDMxLjI3LTUxLjc4bDE4LjY5LTMwLjg3YzI0Ljg4LTQxLjE5IDQ0Ljg4LTcxLjIzIDc2LjQzLTExNC43NiAxNy0yMy40MyAzMy4xMi00NCA1OC42Ni03NS45MmwuMDYtLjA2IDEuMjYtMS41OS40OS0uNjNjMTAuMzctMTMuMjIgMjAuODQtMjYuMjUgMzEuMTEtMzguNzNsLjM0LS40Mi4yLS4yNWMyMy44My0yOS41MSA0OC4xMi01OS4xMyA3Mi4xOS04OGw5LjU3LTExLjQ4VjM1Ni41NEgxMjY3Yy05LjIzLTguNjgtMTguNjYtMTcuMy0yOC4wOC0yNS42OS03Ni4zMi02Ny43Mi0xNTIuNDMtMTIwLjg1LTIyNi4yNy0xNTcuNzktNzUuNzEtMzcuOTItMTQ1LjYzLTU3LjE0LTIwNy44LTU3LjE0LTQ0LjM5IDAtODQuMDYgOS45NC0xMTcuOTEgMjkuNTMtMzIuNTUgMTguODQtNTkuOSA0Ni45MS04MS4zMSA4My40NC0yMC4wNiAzNC4yMy0zNC44NCA3NS44OS00My45NSAxMjMuODEtMTcuNTQgOTIuMzctMTMuNzQgMjA4IDExIDMzNC4zNCAzLjEyIDE1LjkgNi41NiAzMiAxMC4yNSA0OC0xOS43NCA2LTM4LjkxIDEyLjE4LTU3LjEzIDE4LjUzLTExOS4yNiA0MS41OC0yMTkgOTUuOTEtMjg4LjQyIDE1Ny4xNC0zNiAzMS43MS02My44NCA2NS4yMy04Mi44OSA5OS42Mi0yMC4yNCAzNi41Ny0zMC41MSA3NC4wOC0zMC41MSAxMTEuNDkgMCA3NSA0MS41NSAxNTEgMTIwLjE1IDIxOS45NCA3NCA2NC45MyAxNzguNjUgMTIxLjYyIDMwMi41NSAxNjQgMTEuNTEgMy45MSAyMy4yMiA3LjcxIDM1IDExLjM0LTQuMTIgMTcuOTMtNy45MyAzNi0xMS4zNiA1NC0yMy41NiAxMjQuMDgtMjYuMzEgMjM3LjYyLTggMzI4LjM1IDkuNTEgNDcgMjQuNjEgODcuODkgNDQuODkgMTIxLjU3IDIxLjU2IDM1LjggNDguOTMgNjMuNDQgODEuMzQgODIuMTMgMzQuMTUgMTkuNjkgNzQuMzQgMjkuNjcgMTE5LjQ1IDI5LjY3IDYxLjQyIDAgMTMwLjA5LTE4LjIxIDIwNC4xMS01NC4xMyA3Mi41Ni0zNS4yMSAxNDYuNjMtODUuNjQgMjIwLjEzLTE0OS44OSAxMS4zMy05LjkgMjMtMjAuNDkgMzQuNjYtMzEuNTNoMTcuNDF2LTE3NC4zOWwtNi4zNC03LjgtMy0zLjY3LTc5LjgyLTk4LjU1LTQ0LjM1LTU0Ljc1Yy0yMi4zOS0yNy42NC01OC4yNS03OC43OS0xMDEtMTQ0LTM0LTUxLjg0LTU4Ljc2LTkzLjQ3LTcwLjUtMTEzLjYzbC0xLjI4LTIuMjFjLTIwLjQ5LTM1LjMzLTM1LjcxLTYzLjc5LTQ3LjkzLTg2LjY0LTE1LjMtMjguNjMtMzAuMzktNTcuODctNDQuOTEtODdsMi42Ni01LjE3em0tODAuODYtMTcyLjg5Yy0xMy41My0zNC43LTI1LjkyLTY5LjEzLTM3LTEwMi44MSAzNC41Ny03LjEzIDcwLjUxLTEzLjUzIDEwNy4zMi0xOS4xMS0xMi4yOCAyMC4xMS0yNC4zNCA0MC40MS0zNiA2MC42NHMtMjMuMTIgNDAuNjktMzQuMzIgNjEuMjh6bS0zOC4yNSA0NjIuMWMxMS40NS0zNC44NiAyNC4zMS03MC41NiAzOC4zOS0xMDYuNTkgMTEuMjkgMjAuNyAyMi44NyA0MS4zIDM0LjU4IDYxLjUzIDEyLjI3IDIxLjE5IDI1IDQyLjQ4IDM3Ljk1IDYzLjU4LTM4LjQ2LTUuMzItNzUuNjItMTEuNTMtMTEwLjkyLTE4LjUxek03MDcuMTggMzk3LjE2YzktNjEuNzQgMjkuMTQtMTA1Ljg3IDU1LjM3LTEyMS4wNiAxMC44OS02LjMxIDI1LjM0LTkuNSA0Mi45My05LjUgMzguNDcgMCA4OC4yNCAxNS4xIDE0My45MSA0My42NyA2MC4zNCAzMSAxMjUuNzcgNzcuMTEgMTg5LjIyIDEzMy40NXE1LjkgNS4yNCAxMi4wNyAxMC44OWMtNTUuODEgNjEuNzEtMTEwLjgxIDEzMC4yOC0xNjMuNjggMjA0LjEtOTAuMDggOC44LTE3Ni42OCAyMS45NC0yNTcuNzYgMzkuMTEtMi45My0xMi44NC01LjY4LTI1Ljc1LTguMi0zOC41bC0uMjQtMS4zMmMtMTkuMzMtOTguNzYtMjQuMDQtMTg5LTEzLjYyLTI2MC44NHptMTEuMzEgNzI3Yy0zOC4xIDg0LTcwLjQ4IDE2Ny4xNi05Ni4zNyAyNDcuNDYtOS0yLjgtMTcuODktNS43Mi0yNi42NS04LjctMTAzLTM1LjIxLTE4OC43OS04MC42Ni0yNDguMDgtMTMxLjQzLTQ1LjM3LTM4Ljg1LTcyLjQ1LTc5LjgzLTcyLjQ1LTEwOS42MyAwLTMwLjU5IDI3LjA5LTcwIDc0LjM0LTEwOC4yIDU1LTQ0LjQ3IDEzMy4yNy04NS4xMSAyMjYuMTktMTE3LjUgMTUuNzktNS40OCAzMS44Ny0xMC43MyA0OC0xNS42NCAyNS41MyA3OS4xMyA1Ny40OCAxNjAuOTkgOTUuMDEgMjQzLjYzem0uMTIgNDc0LjkyYzIuODYtMTUgNi0zMC4wOSA5LjQzLTQ1LjA5IDgwIDE2LjM3IDE2Ny41OSAyOC43MSAyNjAuNzUgMzYuNzEgNTMuMzggNzQuMzQgMTA4Ljg0IDE0My4zNyAxNjUuMDUgMjA1LjQ0LTYuOTQgNi4zOS0xMy45NCAxMi42OC0yMC45IDE4Ljc5LTgxLjM3IDcxLjEzLTE2My4wOSAxMjIuNTUtMjM2LjI4IDE0OC43MWwtMS41Mi41NGMtMzIuOCAxMS41OC02Mi4wOSAxNy40NS04NyAxNy40NS0xOC4zOCAwLTMzLjIyLTMuMTgtNDQuMTEtOS40Ni0yNi41MS0xNS4yOS00Ny4xNC01OC40Ni01Ni42LTExOC40NS0xMS4wNi02OS45MS03LjE5LTE1Ny45NyAxMS4xNy0yNTQuNjV6IgogICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTEyMy45OCAtMTE1LjkyKSIgaWQ9Il9Hcm91cF8iIGRhdGEtbmFtZT0iJmx0O0dyb3VwJmd0OyIgLz4KICA8cGF0aCBkPSJNMjE5NS4wNyAxMjA1LjFxLTU2LjI2LTEwMy40NS0xNjUuNTQtMTM0LjIzYTIuODUgMi44NSAwIDAgMS0xLjc5LTIuODUgMyAzIDAgMCAxIDEuMzctMi42OGM1NC4yNC0zMC4xNiA5NS45My03My4xMSAxMjUuMTUtMTI3LjlxNDQuMTUtODIuODkgNDQuMTYtMTk5LjQ5YzAtMTI4LjI1LTMyLjQ1LTIyMy42NS05Ny4wNi0yODcuMTFzLTE1MC42Mi05NS0yNTcuNy05NWgtNTc4LjM1Yy0xLjI1IDAtMi4yNyAxLjMxLTIuMjcgMi45M1YxODk0LjNjMCAxLjYyIDEgMi45MyAyLjI3IDIuOTNoNTQ2LjU3cTExNS40MiAwIDE5OS4xMS0zMy42OWM1NS45MS0yMi40MiAxMDEuNC01Mi42OCAxMzcuMjEtOTAuNjhzNjEuOTItODMuMzMgNzguNjYtMTM2YzE2Ljc0LTUyLjIzIDI1LjEtMTA4LjggMjUuMS0xNjkuMjQuMDQtMTA1LjM3LTE5LjA4LTE5Mi41Ny01Ni44OS0yNjIuNTJ6bS03MjIuMzItNjI4YzAtMS42MiAxLTIuOTMgMi4yNy0yLjkzaDMzOS40NmM1Ni45NSAwIDEwMC41MSAxNiAxMzAuNjUgNDcuNTVzNDUuMiA4Ni40IDQ1LjIgMTY0LjE4YzAgNjkuMTgtMTYuNzQgMTIxLjkyLTUwLjI1IDE1OS4wOHMtNzUuMzcgNTUuMzItMTI1LjYzIDU1LjMySDE0NzVjLTEuMjUgMC0yLjI3LTEuMzEtMi4yNy0yLjkzem01MTguMTYgMTAzNS44Yy0zNC44NiA0Mi43Ni04NC40MSA2NC0xNDguMDcgNjRIMTQ3NWMtMS4yNSAwLTIuMjctMS4zMS0yLjI3LTIuOTN2LTQ3Ni40YzAtMS42MiAxLTIuOTMgMi4yNy0yLjkzaDM2Ny44MmM2My42NiAwIDExMi41NyAxOS44OSAxNDguMDcgNjEuMzVzNTIuOTMgOTkuODIgNTIuOTMgMTc2LjNjLjAyIDc3Ljc3LTE3Ljc2IDEzOC4yNy01Mi45MSAxODAuNjF6IgogICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTEyMy45OCAtMTE1LjkyKSIgLz48L3N2Zz4K",
    },
    {
        name: "TypeScript",
        desc: "JavaScript variant with types",
        link: "https://www.typescriptlang.org/",
        image: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/1200px-Typescript_logo_2020.svg.png",
    },
    {
        name: "Flask",
        desc: "Web framework written in Python",
        link: "https://flask.palletsprojects.com/en/2.0.x/",
        image: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Flask_logo.svg/1280px-Flask_logo.svg.png",
    },
    {
        name: "SQLAlchemy",
        desc: "SQL toolkit for python",
        link: "https://www.sqlalchemy.org/",
        image: "https://quintagroup.com/cms/python/images/sqlalchemy-logo.png/@@images/eca35254-a2db-47a8-850b-2678f7f8bc09.png",
    },
    {
        name: "Postgres",
        desc: "Database management system for SQL",
        link: "https://www.postgresql.org/",
        image: "http://assets.stickpng.com/images/584815fdcef1014c0b5e497a.png",
    },
    {
        name: "Black",
        desc: "Python code formatter",
        link: "https://black.readthedocs.io/en/stable/",
        image: "https://raw.githubusercontent.com/psf/black/main/docs/_static/logo2-readme.png",
    },
    {
        name: "Prettier",
        desc: "Code formatter for JavaScript",
        link: "https://prettier.io/",
        image: "https://user-images.githubusercontent.com/1022054/59317198-f1149b80-8d15-11e9-9b0f-0c5e7d4b8b81.png",
    },
    {
        name: "AWS",
        desc: "Website host",
        link: "https://aws.amazon.com/",
        image: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1024px-Amazon_Web_Services_Logo.svg.png",
    },
    {
        name: "Postman",
        desc: "RESTful API documentor",
        link: "https://www.postman.com/",
        image: "https://iconape.com/wp-content/png_logo_vector/postman-logo.png",
    },
    {
        name: "Docker",
        desc: "Container platform for deployment and testing",
        link: "https://www.docker.com/",
        image: "https://www.docker.com/sites/default/files/d8/2019-07/vertical-logo-monochromatic.png",
    },
    {
        name: "Gitlab",
        desc: "Code repository service",
        link: "https://gitlab.com/",
        image: "https://docs.gitlab.com/assets/images/gitlab-logo.svg",
    },
    {
        name: "Namecheap",
        desc: "Domain name service",
        link: "https://www.namecheap.com/",
        image: "https://www.logo.wine/a/logo/Namecheap/Namecheap-Logo.wine.svg",
    },
    {
        name: "Discord",
        desc: "Team communication platform",
        link: "https://discord.com/",
        image: "https://logos-world.net/wp-content/uploads/2020/12/Discord-Logo.png",
    },
    {
        name: "Selenium",
        desc: "GUI testing framework",
        link: "https://www.selenium.dev/",
        image: "https://upload.wikimedia.org/wikipedia/commons/d/d5/Selenium_Logo.png",
    },
    {
        name: "Jest",
        desc: "Javascript testing framework",
        link: "https://jestjs.io/",
        image: "https://camo.githubusercontent.com/ec4626e44870f03423673ea299ceb6f37afa7f9bf848ca5ad095feca41f230b6/68747470733a2f2f6c616e64696e672d706167652d626f6f6b2e66726f6e7431302e636f6d2f696d616765732f6672616d65776f726b732f6a6573742e706e67",
    },
    {
        name: "Pytest",
        desc: "Python unit testing framework",
        link: "https://docs.pytest.org/en/6.2.x/",
        image: "https://warehouse-camo.ingress.cmh1.psfhosted.org/1599e7e4caeaac6ca1a8d4ace3cefa8a0d160925/68747470733a2f2f6769746875622e636f6d2f7079746573742d6465762f7079746573742f7261772f6d61696e2f646f632f656e2f696d672f7079746573745f6c6f676f5f6375727665732e737667",
    },
    {
        name: "React Multiselect Dropdown",
        desc: "Dropdown select box that allows multiple selections",
        link: "https://www.npmjs.com/package/multiselect-react-dropdown",
        image: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0xMS41IC0xMC4yMzE3NCAyMyAyMC40NjM0OCI+CiAgPHRpdGxlPlJlYWN0IExvZ288L3RpdGxlPgogIDxjaXJjbGUgY3g9IjAiIGN5PSIwIiByPSIyLjA1IiBmaWxsPSIjNjFkYWZiIi8+CiAgPGcgc3Ryb2tlPSIjNjFkYWZiIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIi8+CiAgICA8ZWxsaXBzZSByeD0iMTEiIHJ5PSI0LjIiIHRyYW5zZm9ybT0icm90YXRlKDYwKSIvPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIiB0cmFuc2Zvcm09InJvdGF0ZSgxMjApIi8+CiAgPC9nPgo8L3N2Zz4K",
    },
];

interface IIssueStats {
    total: number;
    developers: Record<string, number>;
}

interface ICommitStats {
    total: number;
    developers: Record<string, number>;
}

interface IRepositoryStats {
    totalIssues: number;
    totalCommits: number;
    totalTests: number;
}

interface IDeveloperStats {
    issues: number;
    commits: number;
    tests: number;
}

interface IAboutPageProps {}

interface IAboutPageState {
    repository: IRepositoryStats;
    developers: Record<string, IDeveloperStats>;
}

class AboutPage extends React.Component<IAboutPageProps, IAboutPageState> {
    constructor(props: IAboutPageProps) {
        super(props);

        let developerMap: Record<string, IDeveloperStats> = {};
        let totalTests = 0;

        for (let i = 0; i < developers.length; i++) {
            developerMap[developers[i].name] = {
                issues: 0,
                commits: 0,
                tests: developers[i].tests,
            };

            totalTests += developers[i].tests;
        }

        this.state = {
            repository: {
                totalIssues: 0,
                totalCommits: 0,
                totalTests: totalTests,
            },
            developers: developerMap,
        };
    }

    async fetchAll(url: string) {
        let response = await fetch(url + "?per_page=100&page=1");
        let data = await response.json();
        let headers = response.headers;
        let nextPage = headers.get("X-Next-Page");

        while (nextPage != null && nextPage !== "") {
            let page: number = parseInt(nextPage);

            response = await fetch(url + "?per_page=100&page=" + page);
            data.push(...(await response.json()));
            headers = response.headers;
            nextPage = headers.get("X-Next-Page");
        }

        return data;
    }

    async fetchIssues(): Promise<IIssueStats> {
        let data = await this.fetchAll(
            gitlabApiUrl + repository.id + "/issues"
        );

        let developerIssues: Record<string, number> = {};

        for (let i = 0; i < developers.length; i++) {
            developerIssues[developers[i].name] = 0;
        }

        for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < data[i].assignees.length; j++) {
                let assignee = data[i].assignees[j].username;
                for (let k = 0; k < developers.length; k++) {
                    if (developers[k].gitlabName === assignee) {
                        developerIssues[developers[k].name] += 1;
                    }
                }
            }
        }

        return {
            total: data.length,
            developers: developerIssues,
        };
    }

    async fetchCommits(): Promise<ICommitStats> {
        let data = await this.fetchAll(
            gitlabApiUrl + repository.id + "/repository/commits"
        );

        let developerCommits: Record<string, number> = {};

        for (let i = 0; i < developers.length; i++) {
            developerCommits[developers[i].name] = 0;
        }

        for (let i = 0; i < data.length; i++) {
            let committer = data[i].committer_name;
            for (let j = 0; j < developers.length; j++) {
                if (developers[j].gitlabCommitName === committer) {
                    developerCommits[developers[j].name] += 1;
                }
            }
        }

        return {
            total: data.length,
            developers: developerCommits,
        };
    }

    async componentDidMount() {
        let issuesPromise = this.fetchIssues();
        let commitsPromise = this.fetchCommits();

        let issues = await issuesPromise;
        let commits = await commitsPromise;

        let repositoryState = {
            totalIssues: issues.total,
            totalCommits: commits.total,
            totalTests: this.state.repository.totalTests,
        };
        let developersState: Record<string, IDeveloperStats> = {};

        for (let i = 0; i < developers.length; i++) {
            developersState[developers[i].name] = {
                issues: issues.developers[developers[i].name],
                commits: commits.developers[developers[i].name],
                tests: developers[i].tests,
            };
        }

        this.setState({
            repository: repositoryState,
            developers: developersState,
        });
    }

    render() {
        let repositoryCards: ICardProps[] = [
            {
                header: "Gitlab Repository",
                text:
                    "Commits: " +
                    this.state.repository.totalCommits +
                    "\nIssues: " +
                    this.state.repository.totalIssues +
                    "\nTests: " +
                    this.state.repository.totalTests,
                buttonText: "View Repository",
                buttonLink: repository.link,
            },
            {
                header: "Postman API Documentation",
                text: "\n\n\n",
                buttonText: "View API",
                buttonLink: repository.apiLink,
            },
        ];

        let developerCards: ICardProps[] = developers.map((developer) => {
            return {
                width: 25,
                image: developer.image,
                title: developer.name + " (" + developer.role + ")",
                text: developer.bio,
                footer:
                    "Commits: " +
                    this.state.developers[developer.name].commits +
                    "\nIssues: " +
                    this.state.developers[developer.name].issues +
                    "\nTests: " +
                    this.state.developers[developer.name].tests,
                buttonText: "View Gitlab",
                buttonLink: developer.gitlabLink,
            };
        });

        let apiCards: ICardProps[] = apis.map((api) => {
            return {
                image: api.image,
                imageFull: true,
                imageMargin: true,
                imageDark: true,
                title: api.name,
                text: api.desc,
                clickable: true,
                clickableLink: api.link,
            };
        });

        let toolCards: ICardProps[] = tools.map((tool) => {
            return {
                image: tool.image,
                imageFull: true,
                imageMargin: true,
                imageDark: true,
                title: tool.name,
                text: tool.desc,
                clickable: true,
                clickableLink: tool.link,
            };
        });

        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="about-title">About Cities Love Baseball</h1>
                <br />
                <br />
                <div className="about-text">
                    Cities Love Baseball is a project dedicated to getting
                    people excited about playing baseball. We gather and provide
                    information about baseball teams, baseball players, and
                    cities to get the users of our site interested in baseball.
                    We hope that by getting people interested in baseball, they
                    will be more likely to play it themselves and thus be both
                    happier and healthier.
                </div>
                <br />
                <br />
                <h2 className="about-title">Repository Information</h2>
                <br />
                <br />
                <CardGrid cards={repositoryCards} />
                <br />
                <br />
                <h2 className="about-title">Developers</h2>
                <br />
                <br />
                <CardGrid cards={developerCards} />
                <br />
                <br />
                <h2 className="about-title">APIs and Datasources</h2>
                <br />
                <br />
                <CardGrid cards={apiCards} />
                <br />
                <br />
                <h2 className="about-title">Tools</h2>
                <br />
                <br />
                <CardGrid cards={toolCards} />
                <br />
                <br />
            </div>
        );
    }
}

export default AboutPage;
