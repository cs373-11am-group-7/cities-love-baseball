import React from "react";
import "./SplashPage.css";
import Card from "../components/Card";
import splashImage from "../images/splash-image.jpg";
import city from "../images/city-card.jpg";
import team from "../images/team-card.jpg";
import player from "../images/player-card.jpg";
import SearchBar from "../components/SearchBar";

class SplashPage extends React.Component {
    render() {
        return (
            <div>
                <img className="splash-image" src={splashImage} alt="" />
                <div className="splash-content">
                    <br />
                    <br />
                    <br />
                    <h1 className="splash-title">Cities Love Baseball</h1>
                    <br />
                    <h2 className="splash-title">
                        Get informed about baseball and see why so many people
                        enjoy playing it
                    </h2>
                    <br />
                    <br />
                    <br />
                    <SearchBar modelLink={""} />
                    <br />
                    <br />
                    <br />
                    <div className="splash-cards">
                        <Card
                            image={city}
                            title="Cities"
                            text="Learn information about cities and their relation to baseball"
                            buttonText="View"
                            buttonLink="/cities/view"
                        />
                        <Card
                            image={team}
                            title="Teams"
                            text="Learn information about baseball teams and view their performance"
                            buttonText="View"
                            buttonLink="/teams/view"
                        />
                        <Card
                            image={player}
                            title="Players"
                            text="Learn information about baseball players and their careers"
                            buttonText="View"
                            buttonLink="/players/view"
                        />
                    </div>
                    <br />
                    <br />
                </div>
            </div>
        );
    }
}

export default SplashPage;
