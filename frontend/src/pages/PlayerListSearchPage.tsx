import React from "react";
import "./PlayerListPage.css";
import ModelGrid from "../components/ModelGrid";
import SearchBar from "../components/SearchBar";

class PlayerListPage extends React.Component {
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="player-list-title">Players List Search</h1>
                <br />
                <br />
                <SearchBar modelLink={"players"} />
                <br />
                <ModelGrid
                    modelLink="players"
                    backendLink="player"
                    backendData="players"
                    disableFilter
                    fields={[
                        {
                            name: "team.name",
                            display: "Team",
                        },
                        {
                            name: "city.name",
                            display: "City",
                        },
                        {
                            name: "birth_city",
                            display: "Birth City",
                        },
                        {
                            name: "position",
                            display: "Position",
                        },
                        {
                            name: "batting_side",
                            display: "Batting Side",
                        },
                        {
                            name: "pitching_hand",
                            display: "Pitching Hand",
                        },
                    ]}
                    filters={[]}
                    sorts={[]}
                />
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default PlayerListPage;
