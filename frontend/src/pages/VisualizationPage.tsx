import React from "react";
import "./VisualizationPage.css";
import * as d3 from "d3";
import ITeam from "../interfaces/ITeam";
import ICity from "../interfaces/ICity";
import { backendApiUrl } from "../Constants";
import IPlayer from "../interfaces/IPlayer";
import usa from "../data/usa.json";
import LoadingCircle from "../components/LoadingCircle";

interface IProps {}

interface IState {
    loading: boolean;
}

export default class VisualizationPage extends React.Component<IProps, IState> {
    private readonly v1Ref = React.createRef<SVGSVGElement>();
    private readonly v2Ref = React.createRef<SVGSVGElement>();
    private readonly v3Ref = React.createRef<SVGSVGElement>();

    constructor(props: IProps) {
        super(props);

        this.state = {
            loading: true,
        };
    }

    renderVisualizationOne(data: [ITeam, ICity][]) {
        const width = 1000;
        const height = 600;

        const marginTop = 20;
        const marginRight = 30;
        const marginBottom = 30;
        const marginLeft = 40;

        const radius = 3;

        const inset = radius * 2;

        const xRange = [marginLeft + inset, width - marginRight - inset];
        const yRange = [height - marginBottom - inset, marginTop + inset];

        const fill = "none";
        const stroke = "currentColor";
        const strokeWidth = 1.5;

        const X = d3.map(data, (d) => d[1].poverty_rate);
        const Y = d3.map(data, (d) => d[0].runs / d[0].games_played);
        const I = d3
            .range(X.length)
            .filter((i) => !isNaN(X[i]) && !isNaN(Y[i]));

        const xDomain = d3.extent(X) as [number, number];
        const yDomain = d3.extent(Y) as [number, number];

        const xScale = d3.scaleLinear(xDomain, xRange);
        const yScale = d3.scaleLinear(yDomain, yRange);
        const xAxis = d3.axisBottom(xScale).ticks(width / 80);
        const yAxis = d3.axisLeft(yScale).ticks(height / 50);

        const svg = d3
            .select(this.v1Ref.current)
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", `0 0 ${width} ${height}`)
            .attr(
                "style",
                "max-width: 100%; height: auto; height: intrinsic; color: white"
            );

        svg.append("g")
            .attr("transform", `translate(0,${height - marginBottom})`)
            .call(xAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick link")
                    .clone()
                    .attr("y2", marginTop + marginBottom - height)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", width)
                    .attr("y", marginBottom - 4)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "end")
                    .text("Poverty Rate")
            );

        svg.append("g")
            .attr("transform", `translate(${marginLeft},0)`)
            .call(yAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick link")
                    .clone()
                    .attr("x2", width - marginLeft - marginRight)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", -marginLeft)
                    .attr("y", 10)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "start")
                    .text("Runs Per Game")
            );

        svg.append("g")
            .attr("fill", fill)
            .attr("stroke", stroke)
            .attr("stroke-width", strokeWidth)
            .selectAll("circle")
            .data(I)
            .join("circle")
            .attr("cx", (i) => xScale(X[i]))
            .attr("cy", (i) => yScale(Y[i]))
            .attr("r", radius);
    }

    renderVisualizationTwo(data: IPlayer[]) {
        const width = 1000;
        const height = 600;

        const marginTop = 20;
        const marginRight = 30;
        const marginBottom = 30;
        const marginLeft = 40;

        const radius = 3;

        const inset = radius * 2;

        const xRange = [marginLeft + inset, width - marginRight - inset];
        const yRange = [height - marginBottom - inset, marginTop + inset];

        const color = "steelBlue";

        const X = d3.map(data, (d: IPlayer) => {
            let split = d.height.substring(0, d.height.length - 1).split("' ");

            let feet = split[0];
            let inches = split[1];

            return parseInt(feet) * 12 + parseInt(inches);
        });
        const I = d3.range(X.length);

        const bins = d3
            .bin()
            .thresholds((data, min, max) => {
                let result = [];

                for (let i = min; i <= max; i++) {
                    result.push(i);
                }

                return result;
            })
            .value((i) => X[i])(I);

        const xDomain = [
            bins[0].x0 as number,
            bins[bins.length - 1].x1 as number,
        ];
        const yDomain = [0, d3.max(bins, (I) => I.length) as number];

        const xScale = d3.scaleLinear(xDomain, xRange);
        const yScale = d3.scaleLinear(yDomain, yRange);
        const xAxis = d3.axisBottom(xScale).ticks(width / 80);
        const yAxis = d3.axisLeft(yScale).ticks(height / 50);

        const svg = d3
            .select(this.v2Ref.current)
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", `0 0 ${width} ${height}`)
            .attr(
                "style",
                "max-width: 100%; height: auto; height: intrinsic; color: white"
            );

        svg.append("g")
            .attr("transform", `translate(${marginLeft},0)`)
            .call(yAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick link")
                    .clone()
                    .attr("x2", width - marginLeft - marginRight)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", -marginLeft)
                    .attr("y", 10)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "start")
                    .text("Frequency")
            );

        svg.append("g")
            .attr("fill", color)
            .selectAll("rect")
            .data(bins)
            .join("rect")
            .attr("x", (d) => xScale(d.x0 as number) + inset)
            .attr("width", (d) =>
                Math.max(
                    0,
                    xScale(d.x1 as number) -
                        xScale(d.x0 as number) -
                        inset -
                        inset
                )
            )
            .attr("y", (d) => yScale(d.length))
            .attr("height", (d) => yScale(0) - yScale(d.length))
            .append("title");

        svg.append("g")
            .attr("transform", `translate(0,${height - marginBottom})`)
            .call(xAxis)
            .call((g) =>
                g
                    .append("text")
                    .attr("x", width - marginRight)
                    .attr("y", 27)
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "end")
                    .text("Height (Inches)")
            );

        let leftX = marginLeft + inset;
        let rightX = width - marginRight - inset;

        let leftVal = xDomain[0];
        let rightVal = xDomain[1];

        let val = 69;

        let x =
            ((rightX - leftX) / (rightVal - leftVal)) * (val - leftVal) + leftX;

        svg.append("line")
            .style("stroke", "red")
            .style("stroke-width", 1)
            .attr("x1", x)
            .attr("y1", height - marginBottom)
            .attr("x2", x)
            .attr("y2", marginTop);

        svg.append("text")
            .style("fill", "red")
            .style("text-anchor", "middle")
            .style("font-size", "10px")
            .attr("x", x)
            .attr("y", marginTop - 10)
            .text("Average Height (U.S. Male)");
    }

    renderVisualizationThree(data: [ITeam, ICity][]) {
        const width = 2000;
        const height = 1000;

        const radius = 5;

        const fill = "red";
        const stroke = "red";
        const strokeWidth = 2;

        const svg = d3
            .select(this.v3Ref.current)
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", `0 0 ${width} ${height}`)
            .attr(
                "style",
                "max-width: 100%; height: auto; height: intrinsic; color: white"
            );

        const projection = d3
            .geoAlbersUsa()
            .scale(width)
            .translate([width / 2, height / 2]);

        svg.append("g")
            .selectAll("path")
            .data(usa.features)
            .enter()
            .append("path")
            .attr("fill", "#69b3a2")
            .attr("d", d3.geoPath().projection(projection) as any)
            .style("stroke", "#fff");

        svg.append("g")
            .attr("fill", fill)
            .attr("stroke", stroke)
            .attr("stroke-width", strokeWidth)
            .selectAll("circle")
            .data(data)
            .join("circle")
            .attr(
                "transform",
                (d) =>
                    "translate(" +
                    projection([d[1].longitude, d[1].latitude]) +
                    ")"
            )
            .attr("r", radius);
    }

    async fetchCity(cityId: number): Promise<ICity> {
        let cityResponse = await fetch(backendApiUrl + "/city/id=" + cityId);
        let cityData = await cityResponse.json();

        return cityData as ICity;
    }

    async fetchTeams(): Promise<ITeam[]> {
        let response = await fetch(
            backendApiUrl + "/team?page=1&pageSize=10000"
        );
        let data = await response.json();
        let teams = data.teams as ITeam[];

        return teams;
    }

    async fetchTeamCities(): Promise<[ITeam, ICity][]> {
        let teams = await this.fetchTeams();
        teams = teams.filter((team) => team.city.id !== -1);

        let cityProms: Promise<ICity>[] = [];

        for (let team of teams) {
            console.log(team.city.id === -1);
            cityProms.push(this.fetchCity(team.city.id));
        }

        let cities: ICity[] = [];

        for (let cityProm of cityProms) {
            cities.push(await cityProm);
        }

        let teamCities: [ITeam, ICity][] = [];

        for (let i = 0; i < teams.length; i++) {
            teamCities.push([teams[i], cities[i]]);
        }

        return teamCities;
    }

    async fetchPlayers(): Promise<IPlayer[]> {
        let response = await fetch(
            backendApiUrl + "/player?page=1&pageSize=10000"
        );
        let data = await response.json();
        let players = data.players as IPlayer[];

        return players;
    }

    async fetchTeamPlayers(): Promise<[ITeam, IPlayer[]][]> {
        let teams = await this.fetchTeams();
        let players = await this.fetchPlayers();

        let teamPlayers: [ITeam, IPlayer[]][] = [];

        for (let team of teams) {
            let roster: IPlayer[] = [];

            for (let player of players) {
                if (player.team.id === team.id) {
                    roster.push(player);
                }
            }

            teamPlayers.push([team, roster]);
        }

        return teamPlayers;
    }

    async componentDidMount() {
        let teamCityData = await this.fetchTeamCities();
        let playersData = await this.fetchPlayers();

        await this.setState({
            loading: false,
        });

        this.renderVisualizationOne(teamCityData);
        this.renderVisualizationTwo(playersData);
        this.renderVisualizationThree(teamCityData);
    }
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="visualization-title">
                    Cities Love Baseball Visualizations
                </h1>
                <br />
                <br />
                <div>
                    <h2 className="visualization-title">
                        Runs Per Game by Poverty Rate
                    </h2>
                    <LoadingCircle loading={this.state.loading} />
                    <svg ref={this.v1Ref}></svg>
                </div>
                <br />
                <br />
                <div>
                    <h2 className="visualization-title">Player Heights</h2>
                    <LoadingCircle loading={this.state.loading} />
                    <svg ref={this.v2Ref}></svg>
                </div>
                <br />
                <br />
                <div>
                    <h2 className="visualization-title">Team Map</h2>
                    <LoadingCircle loading={this.state.loading} />
                    <svg ref={this.v3Ref}></svg>
                </div>
                <br />
                <br />
            </div>
        );
    }
}
