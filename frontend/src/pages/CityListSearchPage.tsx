import React from "react";
import "./CityListPage.css";
import SearchBar from "../components/SearchBar";
import ModelGrid from "../components/ModelGrid";

class CityListSearchPage extends React.Component {
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="city-list-title">Cities List Search</h1>
                <br />
                <br />
                <SearchBar modelLink={"cities"} />
                <br />
                <ModelGrid
                    modelLink="cities"
                    backendLink="city"
                    backendData="cities"
                    disableFilter
                    fields={[
                        {
                            name: "state",
                            display: "State",
                        },
                        {
                            name: "largest_industry",
                            display: "Largest Industry",
                        },
                    ]}
                    filters={[]}
                    sorts={[]}
                />
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default CityListSearchPage;
