import React from "react";
import "./TeamListPage.css";
import ModelGrid from "../components/ModelGrid";
import SearchBar from "../components/SearchBar";

class TeamListPage extends React.Component {
    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <h1 className="team-list-title">Teams Search Page</h1>
                <br />
                <br />
                <SearchBar modelLink={"teams"} />
                <br />
                <ModelGrid
                    modelLink="teams"
                    backendLink="team"
                    backendData="teams"
                    disableFilter
                    fields={[
                        {
                            name: "short_name",
                            display: "Short Name",
                        },
                        {
                            name: "abbreviation",
                            display: "Abbreviation",
                        },
                        {
                            name: "city.name",
                            display: "City",
                        },
                    ]}
                    filters={[]}
                    sorts={[]}
                />
                <br />
                <br />
                <br />
            </div>
        );
    }
}

export default TeamListPage;
