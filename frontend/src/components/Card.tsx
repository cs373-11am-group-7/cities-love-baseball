import React, { MouseEvent } from "react";
import { default as BootstrapCard } from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import "./Card.css";

export interface ICardListItem {
    name?: string;
    value?: string;
}

export interface ICardProps {
    width?: number;
    image?: string;
    imageFull?: boolean;
    imageMargin?: boolean;
    imageDark?: boolean;
    header?: string;
    title?: JSX.Element | string;
    text?: string;
    footer?: JSX.Element | string;
    buttonText?: string;
    buttonLink?: string;
    clickable?: boolean;
    clickableLink?: string;
}

export class Card extends React.Component<ICardProps> {
    clickCard() {
        return (event: MouseEvent) => {
            if (!this.props.clickable) return;

            window.location.href =
                this.props.clickableLink == null
                    ? ""
                    : this.props.clickableLink;
        };
    }

    render() {
        let width = 18;

        if (this.props.width != null) {
            width = this.props.width;
        }

        let imageClass = "card-image";

        if (this.props.imageFull) {
            if (this.props.imageMargin) {
                if (this.props.imageDark) {
                    imageClass = "card-image-full-margin-dark";
                } else {
                    imageClass = "card-image-full-margin";
                }
            } else {
                imageClass = "card-image-full";
            }
        }

        return (
            <BootstrapCard
                style={{ width: width + "rem" }}
                className={this.props.clickable ? "card-clickable" : "card"}
                border="dark"
                onClick={this.clickCard()}
            >
                {this.props.image != null && (
                    <BootstrapCard.Img
                        className={imageClass}
                        variant="top"
                        src={this.props.image}
                    />
                )}
                {this.props.header != null && (
                    <BootstrapCard.Header>
                        {this.props.header}
                    </BootstrapCard.Header>
                )}
                <BootstrapCard.Body>
                    {this.props.title != null && (
                        <BootstrapCard.Title>
                            {this.props.title}
                        </BootstrapCard.Title>
                    )}
                    {this.props.text != null && (
                        <BootstrapCard.Text className="card-text">
                            {this.props.text}
                        </BootstrapCard.Text>
                    )}
                    {this.props.children}
                    {this.props.footer != null && (
                        <div>
                            <hr />
                            <BootstrapCard.Text className="card-text">
                                {this.props.footer}
                            </BootstrapCard.Text>
                        </div>
                    )}
                    {this.props.buttonText != null &&
                        this.props.buttonLink != null && (
                            <div className="card-button-wrapper">
                                <Button
                                    className="card-button"
                                    variant="primary"
                                    href={this.props.buttonLink}
                                >
                                    {this.props.buttonText}
                                </Button>
                            </div>
                        )}
                </BootstrapCard.Body>
            </BootstrapCard>
        );
    }
}

export default Card;
