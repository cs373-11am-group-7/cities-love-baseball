import React from "react";
import "./CardGrid.css";
import Card, { ICardProps } from "./Card";

interface ICardGridProps {
    cards: ICardProps[];
}

interface ICardGridState {}

class CardGrid extends React.Component<ICardGridProps, ICardGridState> {
    render() {
        return (
            <div className="grid">
                {this.props.cards.map((card, i) => (
                    <Card {...card} />
                ))}
            </div>
        );
    }
}

export default CardGrid;
