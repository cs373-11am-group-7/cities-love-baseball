import React from "react";
import "./ModelGrid.css";
import FilterBar from "./FilterBar";
import GridPagination from "./GridPagination";
import { getParameterValue } from "./Pagination";

interface IField {
    name: string;
    display: string;
}

interface IValue {
    display: string;
    value: string;
}

interface IFilter {
    name: string;
    display: string;
    values: IValue[];
}

interface IProps {
    modelLink: string;
    backendLink: string;
    backendData: string;
    fields: IField[];
    filters: IFilter[];
    sorts: IValue[];
    disableFilter?: boolean;
}

interface IState {
    sortBy: string;
    sortDir: number;
    filterBy: string;
    filterValues: string;
    searchFor: string;
}

class ModelGrid extends React.Component<IProps, IState> {
    private readonly paginationRef = React.createRef<GridPagination>();

    constructor(props: IProps) {
        super(props);

        this.state = {
            sortBy: "",
            sortDir: 0,
            filterBy: "",
            filterValues: "",
            searchFor: "",
        };

        this.getItemData = this.getItemData.bind(this);
        this.onFilterChange = this.onFilterChange.bind(this);
        this.onSortChange = this.onSortChange.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
    }

    highlight(text: string): JSX.Element {
        const search = this.state.searchFor.replace("%20", " ");

        if (search === "") {
            return <span>{text}</span>;
        }

        let textParts = text.split(new RegExp(`(${search})`, "gi"));

        return (
            <span>
                {textParts.map((part: string) => {
                    if (part.toLowerCase() === search.toLowerCase()) {
                        return (
                            <span className="model-grid-highlight">{part}</span>
                        );
                    } else {
                        return part;
                    }
                })}
            </span>
        );
    }

    getFieldValue(item: any, field: IField): JSX.Element {
        let keys = field.name.split(".");
        let value = item;

        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            value = value[key];
        }

        if (value === null) {
            return <span>No Data</span>;
        }

        if (value.toLocaleString !== undefined) {
            value = value.toLocaleString();
        }

        let text: string = value.toString();
        return this.highlight(text);
    }

    getItemData(item: any, i: number) {
        let title = this.highlight(item.name);

        let display = (
            <div>
                {this.props.fields.map((field) => {
                    return (
                        <div>
                            {field.display + ": "}{" "}
                            {this.getFieldValue(item, field)} {"\n"}
                        </div>
                    );
                })}
            </div>
        );

        return {
            image: item.logo_url,
            imageFull: true,
            imageMargin: true,
            title: title,
            footer: display,
            clickable: true,
            clickableLink: "/" + this.props.modelLink + "/view/" + item.id,
        };
    }

    async onFilterChange(filterBy: string, filterValues: string) {
        await this.setState({
            filterBy: filterBy,
            filterValues: filterValues,
        });

        await this.paginationRef.current?.fetchItems();
    }

    async onSortChange(sortBy: string, sortDir: number) {
        await this.setState({
            sortBy: sortBy,
            sortDir: sortDir,
        });

        await this.paginationRef.current?.fetchItems();
    }

    async onSearchChange(searchFor: string) {
        await this.setState({
            searchFor: searchFor,
        });

        await this.paginationRef.current?.fetchItems();
    }

    async componentDidMount() {
        let sortBy = getParameterValue("sortBy");
        let sortDirRaw = getParameterValue("sortDir");

        if (sortBy !== "" && sortDirRaw !== "") {
            let sortDir = 0;

            if (sortDirRaw === "asc") {
                sortDir = 1;
            } else if (sortDirRaw === "desc") {
                sortDir = 2;
            }

            await this.setState({
                sortBy: sortBy,
                sortDir: sortDir,
            });
        }

        let filterBy = getParameterValue("filterBy");
        let filterValues = getParameterValue("filterValues");

        if (filterBy !== "" && filterValues !== "") {
            await this.setState({
                filterBy: filterBy,
                filterValues: filterValues,
            });
        }

        let searchFor = getParameterValue("searchFor");

        if (searchFor !== "") {
            await this.setState({
                searchFor: searchFor,
            });
        }

        await this.paginationRef.current?.fetchItems();
    }

    render() {
        return (
            <div>
                {!this.props.disableFilter && (
                    <FilterBar
                        filters={this.props.filters}
                        sorts={this.props.sorts}
                        onFilterChange={this.onFilterChange}
                        onSortChange={this.onSortChange}
                        sortBy={this.state.sortBy}
                        sortDir={this.state.sortDir}
                        filterBy={this.state.filterBy}
                        filterValues={this.state.filterValues}
                    />
                )}
                <br />
                <GridPagination
                    ref={this.paginationRef}
                    getItemData={this.getItemData}
                    pageSize={9}
                    backendLink={this.props.backendLink}
                    backendData={this.props.backendData}
                    sortBy={this.state.sortBy}
                    sortDir={this.state.sortDir}
                    filterBy={this.state.filterBy}
                    filterValues={this.state.filterValues}
                    searchFor={this.state.searchFor}
                />
            </div>
        );
    }
}

export default ModelGrid;
