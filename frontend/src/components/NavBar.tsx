import React from "react";
import { default as BootstrapNavbar } from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import "./NavBar.css";

class NavBar extends React.Component {
    render() {
        return (
            <BootstrapNavbar
                collapseOnSelect
                expand="lg"
                bg="primary"
                variant="dark"
            >
                <Container>
                    <BootstrapNavbar.Brand href="/">
                        Cities Love Baseball
                    </BootstrapNavbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="bar">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/about">About</Nav.Link>
                            <Nav.Link href="/cities/view">Cities</Nav.Link>
                            <Nav.Link href="/teams/view">Teams</Nav.Link>
                            <Nav.Link href="/players/view">Players</Nav.Link>
                            <Nav.Link href="/visualizations/clb">
                                Visualizations
                            </Nav.Link>
                            <Nav.Link href="/visualizations/provider">
                                Provider Visualizations
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </BootstrapNavbar>
        );
    }
}

export default NavBar;
