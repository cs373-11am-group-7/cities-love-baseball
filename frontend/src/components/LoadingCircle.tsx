import React from "react";
import "./LoadingCircle.css";

export interface ICardProps {
    loading: boolean;
}

export default class LoadingCircle extends React.Component<ICardProps> {
    render() {
        return (
            <div>
                {this.props.loading && (
                    <div>
                        <div className="loading-circle"></div>
                        <br />
                    </div>
                )}
            </div>
        );
    }
}
