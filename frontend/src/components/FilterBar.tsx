import React from "react";
import "./FilterBar.css";
import Col from "react-bootstrap/Col";
import Multiselect from "multiselect-react-dropdown";

interface IValue {
    display: string;
    value: string;
}

interface IFilter {
    name: string;
    display: string;
    values: IValue[];
}

interface IProps {
    onFilterChange: Function;
    onSortChange?: Function;
    filters: IFilter[];
    sorts?: IValue[];
    filterBy: string;
    filterValues: string;
    sortBy?: string;
    sortDir?: number;
    searchFor?: string;
}

export default class FilterBar extends React.Component<IProps> {
    private readonly sortSelectRef = React.createRef<Multiselect>();

    constructor(props: IProps) {
        super(props);

        this.getSelectedValues = this.getSelectedValues.bind(this);
        this.getFilterNum = this.getFilterNum.bind(this);
        this.getSortValue = this.getSortValue.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onSelectSort = this.onSelectSort.bind(this);
    }

    parseFilter(): Map<string, string> {
        let filterBySplit = this.props.filterBy.split("|");
        let filterValuesSplit = this.props.filterValues.split("|");

        let map = new Map<string, string>();

        for (
            let i = 0;
            i < filterBySplit.length && i < filterValuesSplit.length;
            i++
        ) {
            let filterBy = filterBySplit[i];
            let filterValues = filterValuesSplit[i];

            map.set(filterBy, filterValues);
        }

        return map;
    }

    getSelectedValues(index: number): IValue[] {
        let values: IValue[] = [];
        let filter = this.props.filters[index];
        let currentFilter = this.parseFilter().get(filter.name);

        if (currentFilter === undefined) {
            return values;
        }

        let currentFilterSplit = currentFilter.split(",");

        for (let i = 0; i < filter.values.length; i++) {
            let value = filter.values[i];

            for (let j = 0; j < currentFilterSplit.length; j++) {
                let currentValue = currentFilterSplit[j];

                if (value.value === currentValue) {
                    values.push(value);
                    break;
                }
            }
        }

        return values;
    }

    getFilterNum(index: number) {
        let filterMap = this.parseFilter();

        if (!filterMap.has(this.props.filters[index].name)) {
            return 0;
        }

        let filter = filterMap.get(this.props.filters[index].name);

        if (filter === "") {
            return 0;
        }

        return filter?.split(",").length;
    }

    getSortValue(): string {
        if (
            this.props.sorts === undefined ||
            this.props.sortBy === undefined ||
            this.props.sortDir === undefined
        ) {
            return "None";
        }

        let sortValue = this.props.sortBy + "|" + this.props.sortDir;

        for (let i = 0; i < this.props.sorts.length; i++) {
            let sort = this.props.sorts[i];

            if (sort.value === sortValue) {
                return sort.display;
            }
        }

        return "None";
    }

    onSelect(index: number) {
        return async (selectedList: any[], selectedItem: any) => {
            let selectedValues = selectedList
                .map((selected) => selected.value)
                .sort();
            let newQuery = "";

            for (let i = 0; i < selectedValues.length; i++) {
                if (i !== 0) newQuery += ",";

                newQuery += selectedValues[i];
            }

            let filterMap = this.parseFilter();

            let filterBy = "";
            let filterValues = "";

            for (let i = 0; i < this.props.filters.length; i++) {
                if (i !== 0) {
                    filterBy += "|";
                    filterValues += "|";
                }

                filterBy += this.props.filters[i].name;

                if (i === index) {
                    filterValues += newQuery;
                } else if (filterMap.has(this.props.filters[i].name)) {
                    filterValues += filterMap.get(this.props.filters[i].name);
                }
            }

            await this.props.onFilterChange(filterBy, filterValues);
        };
    }

    async onSelectSort(selectedList: any[], selectedItem: any) {
        await this.sortSelectRef.current?.resetSelectedValues();

        if (this.props.onSortChange !== undefined) {
            let selectedItemSplit = selectedItem.value.split("|");
            let sortBy = selectedItemSplit[0];
            let sortDir = parseInt(selectedItemSplit[1]);

            await this.props.onSortChange(sortBy, sortDir);
        }
    }

    render() {
        return (
            <div className="filter-bar">
                {this.props.filters.map((filter, i) => {
                    return (
                        <Col xs="auto">
                            <div className="filter-text">
                                Filter By: {filter.display}
                            </div>
                            <Multiselect
                                options={filter.values}
                                selectedValues={this.getSelectedValues(i)}
                                displayValue="display"
                                showCheckbox={true}
                                closeOnSelect={false}
                                placeholder={this.getFilterNum(i) + " Selected"}
                                onSelect={this.onSelect(i)}
                                onRemove={this.onSelect(i)}
                                style={{
                                    searchBox: {
                                        background: "white",
                                    },
                                    chips: {
                                        display: "none",
                                    },
                                }}
                            />
                        </Col>
                    );
                })}
                {this.props.sorts && (
                    <Col xs="auto">
                        <div className="filter-text">Sort By:</div>
                        <Multiselect
                            ref={this.sortSelectRef}
                            options={this.props.sorts}
                            displayValue="display"
                            placeholder={this.getSortValue()}
                            singleSelect={true}
                            onSelect={this.onSelectSort}
                            style={{
                                searchBox: {
                                    background: "white",
                                },
                                chips: {
                                    display: "none",
                                },
                            }}
                        />
                    </Col>
                )}
            </div>
        );
    }
}

function rangeFilter(
    min: number,
    max: number,
    increment: number,
    display: Function
): IValue[] {
    let values: IValue[] = [];

    for (let i = min; i < max; i += increment) {
        values.push({
            display: display(i) + " - " + display(i + increment - 1),
            value: i + "-" + (i + increment - 1),
        });
    }

    values.push({
        display: display(max) + "+",
        value: max + "-",
    });

    return values;
}

export function numberFilter(
    min: number,
    max: number,
    increment: number
): IValue[] {
    return rangeFilter(min, max, increment, (num: number) =>
        num.toLocaleString()
    );
}

export function heightFilter(
    min: number,
    max: number,
    increment: number
): IValue[] {
    return rangeFilter(min, max, increment, (num: number) => {
        let feet = Math.floor(num / 12);
        let inches = num % 12;

        return `${feet}' ${inches}"`;
    });
}

export function stringFilter(): IValue[] {
    return [
        {
            display: "A - D",
            value: "A-E",
        },
        {
            display: "E - H",
            value: "E-I",
        },
        {
            display: "I - L",
            value: "I-M",
        },
        {
            display: "M - P",
            value: "M-Q",
        },
        {
            display: "Q - T",
            value: "Q-U",
        },
        {
            display: "U - X",
            value: "U-Y",
        },
        {
            display: "Y - Z",
            value: "Y-",
        },
    ];
}
