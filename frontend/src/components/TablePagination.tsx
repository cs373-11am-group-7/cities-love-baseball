import React from "react";
import "./TablePagination.css";
import Pagination from "./Pagination";
import Table from "react-bootstrap/Table";

interface IProps {
    getTableHeader: Function;
    getItemData: Function;
    pageSize: number;
    backendLink: string;
    backendData: string;
    sortBy: string;
    sortDir: number;
    filterBy: string;
    filterValues: string;
    searchFor: string;
}

class TablePagination extends Pagination<IProps> {
    render() {
        return (
            <div className="table-pagination-wrapper">
                <div className="table-pagination-table">
                    <Table striped bordered hover variant="dark">
                        <thead>{this.props.getTableHeader()}</thead>
                        <tbody>{this.getData()}</tbody>
                    </Table>
                </div>
                <br />
                {super.render()}
            </div>
        );
    }
}

export default TablePagination;
