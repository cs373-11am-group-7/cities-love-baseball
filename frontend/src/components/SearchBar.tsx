import React from "react";
import "./SearchBar.css";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import { getParameterValue } from "./Pagination";

interface IProps {
    modelLink: string;
}

interface IState {
    searchFor: string;
}

export default class SearchBar extends React.Component<IProps, IState> {
    //private searchFor: string = "";

    constructor(props: IProps) {
        super(props);
        this.state = { searchFor: "" };

        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        //this.onButtonPress = this.onButtonPress.bind(this);
    }

    handleChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ searchFor: event.target.value });
    }

    getLink(modelLink: string, searchFor: string) {
        if (modelLink === "") {
            return "/search/?searchFor=" + searchFor;
        }
        return "/" + modelLink + "/search/?searchFor=" + searchFor;
    }

    handleKeyPress(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.key === "Enter") {
            window.location.href = this.getLink(
                this.props.modelLink,
                this.state.searchFor
            );
        }
    }

    async componentDidMount() {
        let searchFor = getParameterValue("searchFor").replace("%20", " ");

        if (searchFor !== "") {
            await this.setState({
                searchFor: searchFor,
            });
        }
    }

    // // search button
    // onButtonPress(click: MouseEvent<HTMLButtonElement>) {
    //     window.location.href = "/?searchFor=" + this.searchFor;
    // }

    render() {
        return (
            <div className="search-bar">
                <FormControl
                    type="text"
                    value={this.state.searchFor}
                    onChange={this.handleChange}
                    onKeyPress={this.handleKeyPress}
                />
                <Button
                    size="lg"
                    href={this.getLink(
                        this.props.modelLink,
                        this.state.searchFor
                    )}
                >
                    Search
                </Button>
            </div>

            // <InputGroup className="mb-3">
            //     <FormControl
            //         placeholder="Search"
            //         aria-label="Search"
            //         aria-describedby="basic-addon2"
            //     />
            // </InputGroup>
        );
    }
}
