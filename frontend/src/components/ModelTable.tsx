import React, { MouseEvent } from "react";
import "./ModelTable.css";
import FilterBar from "./FilterBar";
import TablePagination from "../components/TablePagination";
import sortingNone from "../images/sorting/none.png";
import sortingUp from "../images/sorting/up.png";
import sortingDown from "../images/sorting/down.png";
import { getParameterValue } from "./Pagination";

interface IField {
    name: string;
    display: string;
}

interface IValue {
    display: string;
    value: string;
}

interface IFilter {
    name: string;
    display: string;
    values: IValue[];
}

interface IProps {
    modelLink: string;
    backendLink: string;
    backendData: string;
    fields: IField[];
    filters: IFilter[];
    disableFilter?: boolean;
}

interface IState {
    sortBy: string;
    sortDir: number;
    filterBy: string;
    filterValues: string;
    searchFor: string;
}

class ModelTable extends React.Component<IProps, IState> {
    private readonly paginationRef = React.createRef<TablePagination>();

    constructor(props: IProps) {
        super(props);

        this.state = {
            sortBy: "",
            sortDir: 0,
            filterBy: "",
            filterValues: "",
            searchFor: "",
        };

        this.getTableHeader = this.getTableHeader.bind(this);
        this.getItemData = this.getItemData.bind(this);
        this.onFilterChange = this.onFilterChange.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
    }

    clickRow(id: number) {
        return (event: MouseEvent) => {
            window.location.href = "/" + this.props.modelLink + "/view/" + id;
        };
    }

    clickSorting(field: string) {
        return async (event: MouseEvent) => {
            if (this.state.sortBy === field) {
                await this.setState({
                    sortBy: field,
                    sortDir: (this.state.sortDir + 1) % 3,
                });
            } else {
                await this.setState({
                    sortBy: field,
                    sortDir: 1,
                });
            }

            await this.paginationRef.current?.fetchItems();
        };
    }

    // **********************
    clickSearching(field: string) {
        return async (event: MouseEvent) => {
            if (this.state.searchFor !== field) {
                await this.setState({
                    searchFor: field,
                });
            }

            await this.paginationRef.current?.fetchItems();
        };
    }

    getSortImage(field: string) {
        if (field !== this.state.sortBy || this.state.sortDir === 0) {
            return sortingNone;
        } else if (this.state.sortDir === 1) {
            return sortingUp;
        } else if (this.state.sortDir === 2) {
            return sortingDown;
        }
    }

    getTableHeader() {
        return (
            <tr>
                <th>#</th>
                {this.props.fields.map((field) => (
                    <th>
                        {field.display}{" "}
                        <img
                            src={this.getSortImage(field.name)}
                            className="model-table-sorting"
                            onClick={this.clickSorting(field.name)}
                            alt=""
                        />
                    </th>
                ))}
            </tr>
        );
    }

    getFieldValue(item: any, field: IField): any {
        let keys = field.name.split(".");
        let value = item;

        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            value = value[key];
        }

        if (value.toLocaleString !== undefined) {
            value = value.toLocaleString();
        }

        return value;
    }

    getItemData(item: any, i: number) {
        let currentPage = this.paginationRef.current?.state.currPage;

        if (currentPage === undefined) {
            currentPage = 1;
        }

        return (
            <tr
                key={i}
                className="model-table-row"
                onClick={this.clickRow(item.id)}
            >
                <td>{i + (currentPage - 1) * 10}</td>
                {this.props.fields.map((field) => (
                    <td>{this.getFieldValue(item, field)}</td>
                ))}
            </tr>
        );
    }

    async onFilterChange(filterBy: string, filterValues: string) {
        await this.setState({
            filterBy: filterBy,
            filterValues: filterValues,
        });

        await this.paginationRef.current?.fetchItems();
    }

    async onSearchChange(searchFor: string) {
        await this.setState({
            searchFor: searchFor,
        });

        await this.paginationRef.current?.fetchItems();
    }

    async componentDidMount() {
        let sortBy = getParameterValue("sortBy");
        let sortDirRaw = getParameterValue("sortDir");

        if (sortBy !== "" && sortDirRaw !== "") {
            let sortDir = 0;

            if (sortDirRaw === "asc") {
                sortDir = 1;
            } else if (sortDirRaw === "desc") {
                sortDir = 2;
            }

            await this.setState({
                sortBy: sortBy,
                sortDir: sortDir,
            });
        }

        let filterBy = getParameterValue("filterBy");
        let filterValues = getParameterValue("filterValues");

        if (filterBy !== "" && filterValues !== "") {
            await this.setState({
                filterBy: filterBy,
                filterValues: filterValues,
            });
        }

        let searchFor = getParameterValue("searchFor");

        if (searchFor !== "") {
            await this.setState({
                searchFor: searchFor,
            });
        }

        await this.paginationRef.current?.fetchItems();
    }

    render() {
        return (
            <div>
                {!this.props.disableFilter && (
                    <FilterBar
                        filters={this.props.filters}
                        onFilterChange={this.onFilterChange}
                        filterBy={this.state.filterBy}
                        filterValues={this.state.filterValues}
                    />
                )}
                <br />
                <TablePagination
                    ref={this.paginationRef}
                    getTableHeader={this.getTableHeader}
                    getItemData={this.getItemData}
                    pageSize={10}
                    backendLink={this.props.backendLink}
                    backendData={this.props.backendData}
                    sortBy={this.state.sortBy}
                    sortDir={this.state.sortDir}
                    filterBy={this.state.filterBy}
                    filterValues={this.state.filterValues}
                    searchFor={this.state.searchFor}
                />
            </div>
        );
    }
}

export default ModelTable;
