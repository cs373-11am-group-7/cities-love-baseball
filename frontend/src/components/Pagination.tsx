import React from "react";
import "./Pagination.css";
import { default as BootstrapPagination } from "react-bootstrap/Pagination";
import { backendApiUrl } from "../Constants";
import LoadingCircle from "./LoadingCircle";

interface IProps {
    getItemData: Function;
    pageSize: number;
    backendLink: string;
    backendData: string;
    sortBy: string;
    sortDir: number;
    filterBy: string;
    filterValues: string;
    searchFor: string;
}

interface IItemList {
    items: any[];
    totalNum: number;
}

interface IState {
    itemsCache: Map<string, IItemList>;
    currPage: number;
    loading: boolean;
}

class Pagination<T extends IProps> extends React.Component<T, IState> {
    constructor(props: T) {
        super(props);

        this.state = {
            itemsCache: new Map<string, IItemList>(),
            currPage: 1,
            loading: true,
        };

        this.handlePrevPage = this.handlePrevPage.bind(this);
        this.handleFirstPage = this.handleFirstPage.bind(this);
        this.handleLastPage = this.handleLastPage.bind(this);
        this.handleNextPage = this.handleNextPage.bind(this);
        this.getInsidePageButtons = this.getInsidePageButtons.bind(this);
        this.handleClickMiddlePage = this.handleClickMiddlePage.bind(this);
    }

    getItems(): IItemList {
        let items = this.state.itemsCache.get(this.getQuery());

        if (items === undefined) {
            items = {
                items: [],
                totalNum: 0,
            };
        }

        return items;
    }

    numPages() {
        let items = this.getItems();
        let totalNum = items.totalNum;

        return (
            Math.floor(totalNum / this.props.pageSize) +
            (totalNum % this.props.pageSize === 0 ? 0 : 1)
        );
    }

    async setPage(page: number) {
        await this.setState({
            currPage: page,
        });

        await this.fetchItems();
    }

    getData() {
        let data = this.getItems().items.map((item, i) => {
            return this.props.getItemData(item, i);
        });

        return data;
    }

    getInsidePageButtons() {
        let pageButtons = [];
        let showNum = 9; // num of numbers to always show
        let center = 5;

        if (this.numPages() < showNum) {
            // edge case: no need for ellipsis
            for (let i = 2; i < this.numPages(); i++) {
                // start at 2 b/c 1 already appended
                pageButtons.push(
                    <BootstrapPagination.Item
                        key={i}
                        onClick={() => this.handleClickMiddlePage(i)}
                        active={this.state.currPage === i}
                        className={
                            this.state.currPage === i ? "" : "d-none d-sm-block"
                        }
                    >
                        {i}
                    </BootstrapPagination.Item>
                );
            }
        } else {
            if (this.state.currPage > center) {
                pageButtons.push(
                    <BootstrapPagination.Ellipsis
                        disabled
                        className="d-none d-sm-block"
                    />
                );
            }

            if (this.state.currPage > 2) {
                pageButtons.push(
                    <BootstrapPagination.Ellipsis
                        disabled
                        className="d-sm-none"
                    />
                );
            }

            // push nums
            let leftBound = this.state.currPage - 2;
            let rightBound = this.state.currPage + 3;

            if (this.state.currPage <= center) {
                leftBound = 2;
                rightBound = showNum - 1;
            } else if (this.state.currPage > this.numPages() - center) {
                leftBound = this.numPages() - center - 1;
                rightBound = this.numPages() - 1;
            }

            let displayLargeEllipsis =
                this.state.currPage <= this.numPages() - center;
            let displaySmallEllipsis =
                this.state.currPage <= this.numPages() - 2;

            for (let i = leftBound; i < rightBound + 1; i++) {
                let shouldHideSmall = this.state.currPage !== i;
                let shouldHideLarge = false;

                if (i === rightBound) {
                    shouldHideSmall = shouldHideSmall || displaySmallEllipsis;
                    shouldHideLarge = displayLargeEllipsis;
                }

                pageButtons.push(
                    <BootstrapPagination.Item
                        key={i}
                        onClick={() => this.handleClickMiddlePage(i)}
                        active={this.state.currPage === i}
                        className={
                            (shouldHideSmall ? "d-none d-sm-block " : "") +
                            (shouldHideLarge ? "d-sm-none" : "")
                        }
                    >
                        {i}
                    </BootstrapPagination.Item>
                );
            }

            if (displayLargeEllipsis) {
                // right ellipsis check
                pageButtons.push(
                    <BootstrapPagination.Ellipsis
                        disabled
                        className="d-none d-sm-block"
                    />
                );
            }

            if (displaySmallEllipsis) {
                pageButtons.push(
                    <BootstrapPagination.Ellipsis
                        disabled
                        className="d-sm-none"
                    />
                );
            }
        }

        return pageButtons;
    }

    handlePrevPage() {
        let newPage = this.state.currPage === 1 ? 1 : this.state.currPage - 1;
        this.setPage(newPage);
    }

    handleFirstPage() {
        this.setPage(1);
    }

    handleLastPage() {
        this.setPage(this.numPages());
    }

    handleNextPage() {
        let newPage =
            this.state.currPage === this.numPages()
                ? this.numPages()
                : this.state.currPage + 1;
        this.setPage(newPage);
    }

    handleClickMiddlePage(page: number) {
        this.setPage(page);
    }

    getQuery(): string {
        let page = this.state.currPage;
        let pageSize = this.props.pageSize;

        let sortBy = "none";
        let sortDir = "none";

        if (this.props.sortBy !== "" && this.props.sortDir !== 0) {
            sortBy = this.props.sortBy;

            if (this.props.sortDir === 1) {
                sortDir = "asc";
            } else if (this.props.sortDir === 2) {
                sortDir = "desc";
            }
        }

        let filterBy = this.props.filterBy;
        let filterValues = this.props.filterValues;

        let searchFor = this.props.searchFor;

        return `page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&sortDir=${sortDir}&filterBy=${filterBy}&filterValues=${filterValues}&searchFor=${searchFor}`;
    }

    async fetchItems(): Promise<void> {
        const query = this.getQuery();

        window.history.pushState("", "", "?" + query);

        if (!this.state.itemsCache.has(query)) {
            await this.setState({
                loading: true,
            });

            let response = await fetch(
                backendApiUrl + "/" + this.props.backendLink + "?" + query
            );
            let data = await response.json();

            let items = data[this.props.backendData];
            let totalNum = data["totalNum"];

            if (items === undefined || totalNum === undefined) {
                return;
            }

            let newItemsCache = new Map<string, IItemList>(
                this.state.itemsCache
            );
            newItemsCache.set(query, {
                items: items,
                totalNum: totalNum,
            });

            let totalPages =
                Math.floor(totalNum / this.props.pageSize) +
                (totalNum % this.props.pageSize === 0 ? 0 : 1);
            let page = this.state.currPage;

            if (page > totalPages) {
                page = totalPages;
            }

            if (page < 1) {
                page = 1;
            }

            if (page !== this.state.currPage) {
                await this.setState({
                    currPage: page,
                });

                await this.fetchItems();
            } else {
                this.setState({
                    itemsCache: newItemsCache,
                    loading: false,
                });
            }
        }
    }

    async componentDidMount() {
        let page = parseInt(getParameterValue("page"));

        if (!isNaN(page)) {
            await this.setState({
                currPage: page,
            });
        }
    }

    render() {
        return (
            <div>
                {this.props.children}
                <LoadingCircle loading={this.state.loading} />
                {!this.state.loading && this.numPages() === 0 && (
                    <div className="pagination-no-items">
                        There are no matching items
                    </div>
                )}
                {this.numPages() === 0 || (
                    <div className="pagination-buttons">
                        {!(this.numPages() === 1) && ( // only display if more than one page
                            <BootstrapPagination>
                                <BootstrapPagination.Prev // always display prev arrow and 1 button
                                    onClick={this.handlePrevPage}
                                />
                                <BootstrapPagination.Item
                                    onClick={this.handleFirstPage}
                                    active={this.state.currPage === 1}
                                >
                                    {1}
                                </BootstrapPagination.Item>

                                {this.getInsidePageButtons()}

                                <BootstrapPagination.Item // always display last num button and right arrow
                                    onClick={this.handleLastPage}
                                    active={
                                        this.state.currPage === this.numPages()
                                    }
                                >
                                    {this.numPages()}
                                </BootstrapPagination.Item>

                                <BootstrapPagination.Next
                                    onClick={this.handleNextPage}
                                />
                            </BootstrapPagination>
                        )}
                    </div>
                )}
                <p className="pagination-items-text">
                    Total {this.getItems().totalNum} items
                </p>
            </div>
        );
    }
}

export default Pagination;

export function getParameterValue(paramKey: string): string {
    let linkSplit = window.location.href.split("?");

    if (linkSplit.length < 2) {
        return "";
    }

    let params = linkSplit[1];
    let paramsSplit = params.split("&");

    for (let i = 0; i < paramsSplit.length; i++) {
        let paramSplit = paramsSplit[i].split("=");

        if (paramSplit[0] === paramKey) {
            if (paramSplit.length < 2) {
                return "";
            } else {
                return paramSplit[1];
            }
        }
    }

    return "";
}
