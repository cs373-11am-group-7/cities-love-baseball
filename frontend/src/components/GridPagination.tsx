import "./GridPagination.css";
import Pagination from "./Pagination";
import CardGrid from "./CardGrid";

interface IProps {
    getItemData: Function;
    pageSize: number;
    backendLink: string;
    backendData: string;
    sortBy: string;
    sortDir: number;
    filterBy: string;
    filterValues: string;
    searchFor: string;
}

class GridPagination extends Pagination<IProps> {
    render() {
        return (
            <div>
                <CardGrid cards={this.getData()} />
                <br />
                <div className="grid-pagination-wrapper">{super.render()}</div>
            </div>
        );
    }
}

export default GridPagination;
