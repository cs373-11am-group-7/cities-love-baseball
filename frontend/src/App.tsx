import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NavBar from "./components/NavBar";
import SplashPage from "./pages/SplashPage";
import AboutPage from "./pages/AboutPage";
import TeamListPage from "./pages/TeamListPage";
import TeamInstancePage from "./pages/TeamInstancePage";
import PlayerListPage from "./pages/PlayerListPage";
import PlayerInstancePage from "./pages/PlayerInstancePage";
import CityListPage from "./pages/CityListPage";
import CityInstancePage from "./pages/CityInstancePage";
import CityListSearchPage from "./pages/CityListSearchPage";
import PlayerListSearchPage from "./pages/PlayerListSearchPage";
import TeamListSearchPage from "./pages/TeamListSearchPage";
import SplashSearchPage from "./pages/SplashSearchPage";
import VisualizationPage from "./pages/VisualizationPage";
import ProviderVisualizationPage from "./pages/ProviderVisualizationPage";

function App() {
    return (
        <Router>
            <div className="App">
                <NavBar />
                <Switch>
                    <Route path="/visualizations/clb">
                        <VisualizationPage />
                    </Route>
                    <Route path="/visualizations/provider">
                        <ProviderVisualizationPage />
                    </Route>
                    <Route path="/about">
                        <AboutPage />
                    </Route>
                    <Route
                        path="/teams/view/:id"
                        component={TeamInstancePage}
                    />
                    <Route path="/teams/view">
                        <TeamListPage />
                    </Route>
                    <Route path="/teams/search">
                        <TeamListSearchPage />
                    </Route>
                    <Route
                        path="/players/view/:id"
                        component={PlayerInstancePage}
                    />
                    <Route path="/players/view">
                        <PlayerListPage />
                    </Route>
                    <Route path="/players/search">
                        <PlayerListSearchPage />
                    </Route>
                    <Route
                        path="/cities/view/:id"
                        component={CityInstancePage}
                    />
                    <Route path="/cities/view">
                        <CityListPage />
                    </Route>
                    <Route path="/cities/search">
                        <CityListSearchPage />
                    </Route>
                    <Route path="/search">
                        <SplashSearchPage />
                    </Route>
                    <Route path="/">
                        <SplashPage />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
