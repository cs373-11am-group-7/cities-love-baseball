import requests


def test_city():
    request = requests.get("https://api.citieslovebaseball.me/city").json()
    assert len(request["cities"]) > 0


def test_city_id():
    request = requests.get("https://api.citieslovebaseball.me/city/id=517").json()
    assert request["name"] == "Austin"


def test_team():
    request = requests.get("https://api.citieslovebaseball.me/team").json()
    assert len(request["teams"]) > 0


def test_team_id():
    request = requests.get("https://api.citieslovebaseball.me/team/id=140").json()
    assert request["name"] == "Texas Rangers"


def test_player():
    request = requests.get("https://api.citieslovebaseball.me/player").json()
    assert len(request["players"]) > 0


def test_player_id():
    request = requests.get("https://api.citieslovebaseball.me/player/id=405395").json()
    assert request["name"] == "Albert Pujols"
