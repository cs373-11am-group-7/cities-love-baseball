from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from unidecode import unidecode
import urllib
import json
import sys
import os

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

connection_uri = ""

try:
    with open("database-credentials.json") as f:
        database_credentials = json.load(f)

    connection_uri = (
        "postgresql+psycopg2://"
        + database_credentials["username"]
        + ":"
        + database_credentials["password"]
        + "@"
        + database_credentials["ip"]
        + ":"
        + database_credentials["port"]
        + "/"
        + database_credentials["name"]
    )
except:
    connection_uri = os.environ["DB_URL"]

app.config["SQLALCHEMY_DATABASE_URI"] = connection_uri

db = SQLAlchemy(app)


def height_to_int(height):
    height_split = height[:-1].split("' ")

    feet = int(height_split[0])
    inches = int(height_split[1])

    return feet * 12 + inches


def int_to_height(num):
    feet = num // 12
    inches = num % 12

    return str(feet) + "' " + str(inches) + '"'


class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image_url = db.Column(db.String())
    name = db.Column(db.String())
    teams = db.Column(db.ARRAY(db.Integer))
    team_names = db.Column(db.ARRAY(db.String()))
    players = db.Column(db.ARRAY(db.Integer))
    player_names = db.Column(db.ARRAY(db.String()))
    average_commute_time = db.Column(db.Float)
    median_property_value = db.Column(db.Integer)
    average_population_age = db.Column(db.Integer)
    state = db.Column(db.String())
    population = db.Column(db.Integer)
    poverty_rate = db.Column(db.Float)
    employed_population = db.Column(db.Integer)
    largest_industry = db.Column(db.String())
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)

    def serialize(self):
        return {
            "id": self.id,
            "image_url": self.image_url,
            "name": self.name,
            "teams": list(
                map(
                    lambda team: {"id": team[0], "name": team[1]},
                    zip(self.teams, self.team_names),
                )
            ),
            "players": list(
                map(
                    lambda player: {"id": player[0], "name": player[1]},
                    zip(self.players, self.player_names),
                )
            ),
            "average_commute_time": self.average_commute_time,
            "median_property_value": self.median_property_value,
            "average_population_age": self.average_population_age,
            "state": self.state,
            "population": self.population,
            "poverty_rate": self.poverty_rate,
            "employed_population": self.employed_population,
            "largest_industry": self.largest_industry,
            "longitude": self.longitude,
            "latitude": self.latitude
        }


class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    logo_url = db.Column(db.String())
    facebook_url = db.Column(db.String())
    twitter_url = db.Column(db.String())
    instagram_url = db.Column(db.String())
    youtube_url = db.Column(db.String())
    name = db.Column(db.String())
    short_name = db.Column(db.String())
    abbreviation = db.Column(db.String())
    city = db.Column(db.Integer)
    city_name = db.Column(db.String())
    players = db.Column(db.ARRAY(db.Integer))
    player_names = db.Column(db.ARRAY(db.String()))
    games_played = db.Column(db.Integer)
    runs = db.Column(db.Integer)
    home_runs = db.Column(db.Integer)
    hits = db.Column(db.Integer)
    stolen_bases = db.Column(db.Integer)
    wins = db.Column(db.Integer)

    def serialize(self):
        return {
            "id": self.id,
            "logo_url": self.logo_url,
            "facebook_url": self.facebook_url,
            "twitter_url": self.twitter_url,
            "instagram_url": self.instagram_url,
            "youtube_url": self.youtube_url,
            "name": self.name,
            "short_name": self.short_name,
            "abbreviation": self.abbreviation,
            "city": {"id": self.city, "name": self.city_name},
            "players": list(
                map(
                    lambda player: {"id": player[0], "name": player[1]},
                    zip(self.players, self.player_names),
                )
            ),
            "games_played": self.games_played,
            "runs": self.runs,
            "home_runs": self.home_runs,
            "hits": self.hits,
            "stolen_bases": self.stolen_bases,
            "wins": self.wins
        }


class Player(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image_url = db.Column(db.String())
    highlight_url = db.Column(db.String())
    name = db.Column(db.String())
    city = db.Column(db.Integer)
    city_name = db.Column(db.String())
    team = db.Column(db.Integer)
    team_name = db.Column(db.String())
    age = db.Column(db.Integer)
    birth_city = db.Column(db.String())
    height = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    position = db.Column(db.String())
    batting_side = db.Column(db.String())
    pitching_hand = db.Column(db.String())

    def serialize(self):
        return {
            "id": self.id,
            "image_url": self.image_url,
            "highlight_url": self.highlight_url,
            "name": self.name,
            "city": {"id": self.city, "name": self.city_name},
            "team": {"id": self.team, "name": self.team_name},
            "age": self.age,
            "birth_city": self.birth_city,
            "height": int_to_height(self.height),
            "weight": self.weight,
            "position": self.position,
            "batting_side": self.batting_side,
            "pitching_hand": self.pitching_hand,
        }


def create_database():
    db.drop_all()
    db.create_all()


def make_request(request_url):
    print(request_url)

    # Add this header to make data usa happy
    headers = {"User-Agent": "PostmanRuntime/7.28.4"}

    request = urllib.request.Request(request_url, None, headers)
    response = urllib.request.urlopen(request)
    data = json.loads(response.read())

    return data


measures = "Property%20Value,Population,Average%20Commute%20Time,Median%20Age,Poverty%20Rate,Workforce%20by%20Industry%20and%20Gender"

data_usa_api_url = "https://datausa.io/api/data/"


def get_cities():
    request_url = (
        data_usa_api_url + "?drilldowns=Place&measures=" + measures + "&year=latest"
    )
    data = make_request(request_url)

    return data["data"]


def get_industries():
    request_url = (
        data_usa_api_url
        + "?drilldowns=Place,Industry&measures=Workforce%20by%20Industry%20and%20Gender&year=latest"
    )
    data = make_request(request_url)

    return data["data"]


def populate_cities():
    raw_cities = get_cities()

    cities = []
    cities_dict = {}

    city_id = 1

    for raw_city in raw_cities:
        name, state = raw_city.get("Place").split(", ")

        city = City()
        city.id = city_id
        city.name = name
        city.teams = []
        city.players = []
        city.average_commute_time = raw_city.get("Average Commute Time")
        city.median_property_value = raw_city.get("Property Value")
        city.average_population_age = raw_city.get("Median Age")
        city.state = state
        city.population = raw_city.get("Population")
        city.poverty_rate = raw_city.get("Poverty Rate")
        city.employed_population = raw_city.get("Workforce by Industry and Gender")

        cities.append(city)
        cities_dict[name] = city

        city_id += 1

    industries = get_industries()

    current_city = None

    largest_industry = None
    largest_industry_amount = -1

    for industry in industries:
        name, state = industry.get("Place").split(", ")

        if name != current_city:
            if current_city != None:
                cities_dict[current_city].largest_industry = largest_industry

            current_city = name
            largest_industry = None
            largest_industry_amount = -1

        if industry.get("Workforce by Industry and Gender") > largest_industry_amount:
            largest_industry = industry.get("Industry")
            largest_industry_amount = industry.get("Workforce by Industry and Gender")

    city.largest_industry = largest_industry

    return cities, cities_dict


sports = "1"
mlb_api_url = "https://statsapi.mlb.com/api/v1/"


def get_teams():
    request_url = mlb_api_url + "/teams?sportIds=" + sports
    data = make_request(request_url)

    return data["teams"]


def get_team_stats(team_id):
    request_url = (
        mlb_api_url + "/teams/" + str(team_id) + "/stats?group=hitting&stats=career"
    )
    data = make_request(request_url)

    assert len(data["stats"][0]["splits"]) == 1

    return data["stats"][0]["splits"][0]["stat"]


sports_db_url = "https://www.thesportsdb.com/api/v1/json/1/"


def get_team_logo_and_social(team_name):
    request_url = sports_db_url + "/searchteams.php?t=" + team_name.replace(" ", "%20")
    data = make_request(request_url)

    info = {
        "logo": None,
        "facebook": None,
        "twitter": None,
        "instagram": None,
        "youtube": None,
    }

    for team in data["teams"]:
        if team["strSport"] == "Baseball":
            info["logo"] = team["strTeamBadge"]
            info["facebook"] = team["strFacebook"]
            info["twitter"] = team["strTwitter"]
            info["instagram"] = team["strInstagram"]
            info["youtube"] = team["strYoutube"]
            return info

    return info


def clean_city_name(city_name):
    if city_name == "Bronx" or city_name == "Flushing":
        return "New York"

    return city_name


def get_games(team_id):
    request_url = mlb_api_url + "/teams/" + str(team_id) + "?hydrate=previousSchedule"
    data = make_request(request_url)

    dates = data["teams"][0]["previousGameSchedule"]["dates"]
    games = []

    for date in dates:
        games += date["games"]

    return games


def get_game_highlights(game_id):
    request_url = mlb_api_url + "/game/" + str(game_id) + "/content"
    data = make_request(request_url)

    highlights = data["highlights"]["highlights"]["items"]

    return highlights


player_highlights = {}


def process_team_highlights(team_id):
    games = get_games(team_id)
    highlights = []

    for game in games:
        highlights += get_game_highlights(game["gamePk"])

    for highlight in highlights:
        url = highlight["playbacks"][0]["url"]

        for keyword in highlight["keywordsAll"]:
            if keyword["type"] == "player_id":
                player_highlights[str(keyword["value"])] = url


def populate_teams(cities_dict):
    raw_teams = get_teams()

    teams = []

    for raw_team in raw_teams:
        stats = get_team_stats(raw_team["id"])
        logo_and_social = get_team_logo_and_social(raw_team["name"])
        process_team_highlights(raw_team["id"])

        team = Team()
        team.id = raw_team["id"]
        team.logo_url = logo_and_social["logo"]
        team.facebook_url = logo_and_social["facebook"]
        team.twitter_url = logo_and_social["twitter"]
        team.instagram_url = logo_and_social["instagram"]
        team.youtube_url = logo_and_social["youtube"]
        team.name = raw_team["name"]
        team.short_name = raw_team["shortName"]
        team.abbreviation = raw_team["abbreviation"]
        team.players = []
        team.games_played = stats["gamesPlayed"]
        team.runs = stats["runs"]
        team.home_runs = stats["homeRuns"]
        team.hits = stats["hits"]
        team.stolen_bases = stats["stolenBases"]

        city_name = clean_city_name(raw_team["locationName"])
        city = cities_dict.get(city_name)

        if city != None:
            team.city = city.id
            city.teams.append(team.id)
        else:
            team.city = -1

        team.city_name = city_name

        teams.append(team)

    return teams, raw_teams


def get_players_on_team(team_id):
    request_url = mlb_api_url + "/teams/" + str(team_id) + "/roster"
    data = make_request(request_url)

    return data["roster"]


def get_people(person_ids):
    request_url = mlb_api_url + "/people/?personIds=" + person_ids
    data = make_request(request_url)

    return data["people"]


def get_player_image_and_social(player_name, team_name):
    request_url = (
        sports_db_url + "/searchplayers.php?p=" + player_name.replace(" ", "%20")
    )
    data = make_request(request_url)

    info = {
        "image": None,
        "facebook": None,
        "twitter": None,
        "instagram": None,
        "youtube": None,
    }

    if data.get("player") == None:
        return info

    for player in data["player"]:
        if player["strTeam"] == team_name:
            if player["strCutout"] != None:
                info["image"] = player["strCutout"]
            elif player["strThumb"] != None:
                info["image"] = player["strThumb"]
            elif player["strRender"] != None:
                info["image"] = player["strRender"]

            info["facebook"] = player["strFacebook"]
            info["twitter"] = player["strTwitter"]
            info["instagram"] = player["strInstagram"]
            info["youtube"] = player["strYoutube"]

    return info


def get_players(raw_teams, teams, cities_dict):
    players = []
    people = []

    for raw_team, team in zip(raw_teams, teams):
        city_name = clean_city_name(raw_team["locationName"])
        city = cities_dict.get(city_name)

        team_players = get_players_on_team(raw_team["id"])
        players += team_players

        person_ids = ""

        for team_player in team_players:
            person_ids += str(team_player["person"]["id"]) + ","
            team.players.append(team_player["person"]["id"])

            if city != None:
                team_player["city"] = city.id
                city.players.append(team_player["person"]["id"])
            else:
                team_player["city"] = -1

            team_player["cityName"] = city_name

            image_and_social = get_player_image_and_social(
                team_player["person"]["fullName"], raw_team["name"]
            )

            team_player["imageUrl"] = image_and_social["image"]
            team_player["facebookUrl"] = image_and_social["facebook"]
            team_player["twitterUrl"] = image_and_social["twitter"]
            team_player["instagramUrl"] = image_and_social["instagram"]
            team_player["youtubeUrl"] = image_and_social["youtube"]

        people += get_people(person_ids)

    return players, people


def populate_players(raw_teams, teams, cities_dict):
    raw_players, raw_people = get_players(raw_teams, teams, cities_dict)

    players = []

    for raw_player, raw_person in zip(raw_players, raw_people):
        player = Player()
        player.id = raw_person["id"]
        player.image_url = raw_player["imageUrl"]
        player.highlight_url = player_highlights.get(str(raw_person["id"]))
        # None of the players have filled out social media links
        # player.facebook_url = raw_player['facebookUrl']
        # player.twitter_url = raw_player['twitterUrl']
        # player.instagram_url = raw_player['instagramUrl']
        # player.youtube_url = raw_player['youtubeUrl']
        player.name = raw_person["fullName"]
        player.city = raw_player["city"]
        player.city_name = raw_player["cityName"]
        player.team = raw_player["parentTeamId"]
        player.age = raw_person["currentAge"]
        player.birth_city = raw_person["birthCity"]
        player.height = raw_person["height"]
        player.weight = raw_person["weight"]
        player.position = raw_player["position"]["name"]
        player.batting_side = raw_person["batSide"]["description"]
        player.pitching_hand = raw_person["pitchHand"]["description"]

        players.append(player)

    return players


def populate_database():
    try:

        cities, cities_dict = populate_cities()
        teams, raw_teams = populate_teams(cities_dict)
        players = populate_players(raw_teams, teams, cities_dict)

        # Remove the current data
        db.session.query(City).delete()
        db.session.query(Team).delete()
        db.session.query(Player).delete()

        # Add everything to the database
        db.session.add_all(cities)
        db.session.add_all(teams)
        db.session.add_all(players)

        # Commit the changes
        db.session.commit()
    except:
        db.session.rollback()
        raise


def pull_cities():
    cities = (
        db.session.query(City)
        .filter_by(image_url=None)
        .order_by(City.population.desc())
        .all()
    )
    return cities


def get_city_image(city):
    query = city.name + ", " + city.state + " City"
    query = query.replace(" ", "%20")
    query = unidecode(query)

    request_url = (
        "https://bing-image-search1.p.rapidapi.com/images/search?q="
        + query
        + "&count=1"
    )
    print(request_url)

    headers = {
        "x-rapidapi-host": "bing-image-search1.p.rapidapi.com",
        "x-rapidapi-key": "4859770b67mshbac8a99e037e9d2p1aeaf3jsn57b453d34eea",
    }

    request = urllib.request.Request(request_url, None, headers)
    response = urllib.request.urlopen(request)
    data = json.loads(response.read())

    if not ("value" in data.keys()):
        return ""

    if len(data["value"]) == 0:
        return ""

    image = data["value"][0]["contentUrl"]
    return image


def populate_images():
    amount_to_populate = 50

    try:
        cities = pull_cities()

        for i in range(amount_to_populate):
            if i >= len(cities):
                break

            city = cities[i]
            image = get_city_image(city)
            city.image_url = image

        db.session.commit()

        if len(cities) <= amount_to_populate:
            print("All cities have had their image added.")
        else:
            print(
                str(len(cities) - amount_to_populate)
                + " cities remain. Please rerun after a full hour has passed."
            )

    except:
        db.session.rollback()
        raise


def populate_names():
    try:
        cities = db.session.query(City).all()
        teams = db.session.query(Team).all()
        players = db.session.query(Player).all()

        city_names = {}
        team_names = {}
        player_names = {}

        for city in cities:
            city_names[city.id] = city.name

        for team in teams:
            team_names[team.id] = team.name

        for player in players:
            player_names[player.id] = player.name

        for city in cities:
            print("Updating city " + city.name)

            city.team_names = []
            city.player_names = []

            for team in city.teams:
                city.team_names.append(team_names[team])

            for player in city.players:
                city.player_names.append(player_names[player])

        for team in teams:
            print("Updating team " + team.name)

            team.player_names = []

            for player in team.players:
                team.player_names.append(player_names[player])

        for player in players:
            print("Updating player " + player.name)

            player.team_name = team_names[player.team]

        db.session.commit()

        print("All name lists have been updated")
    except:
        db.session.rollback()
        raise


def convert_heights():
    try:
        players = db.session.query(Player).all()

        for player in players:
            player.height = height_to_int(player.height)

        db.session.commit()
    except:
        db.session.rollback()
        raise

open_weather_url = "http://api.openweathermap.org/"
open_weather_key = "7a07dd537dc8731aa4f5f6ea925411ee"

def get_city_location(city):
    query = city.name + "," + city.state + ",US"
    query = query.replace(" ", "%20")
    query = unidecode(query)

    request_url = (
        open_weather_url + "/geo/1.0/direct?q=" + query + "&limit=1&appid=" + open_weather_key
    )

    data = make_request(request_url)

    if len(data) != 0:
        city.longitude = data[0]['lon']
        city.latitude = data[0]['lat']

def populate_location():
    try:
        cities = db.session.query(City).all()

        for city in cities:
            get_city_location(city)

        db.session.commit()
    except:
        db.session.rollback()
        raise

def get_team_wins(team):
    request_url = (
        mlb_api_url + "/teams/" + str(team.id) + "/stats?group=pitching&stats=career"
    )
    data = make_request(request_url)

    assert len(data["stats"][0]["splits"]) == 1

    team.wins = data["stats"][0]["splits"][0]["stat"]["wins"]

def populate_wins():
    try:
        teams = db.session.query(Team).all()

        for team in teams:
            get_team_wins(team)

        db.session.commit()
    except:
        db.session.rollback()
        raise

if __name__ == "__main__":
    action = ""

    if len(sys.argv) > 1:
        action = sys.argv[1]

    if action == "create":
        create_database()
    elif action == "populate":
        populate_database()
    elif action == "images":
        populate_images()
    elif action == "names":
        populate_names()
    elif action == "height":
        convert_heights()
    elif action == "location":
        populate_location()
    elif action == "wins":
        populate_wins()
    else:
        print('Unknown Action "' + action + '"')
