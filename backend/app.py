from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, and_, or_, not_
from sqlalchemy.sql import base
from database_populator.populator import City, Team, Player
import urllib
import json
import sys
import os

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

connection_uri = ""

try:
    with open("database-credentials.json") as f:
        database_credentials = json.load(f)

    connection_uri = (
        "postgresql+psycopg2://"
        + database_credentials["username"]
        + ":"
        + database_credentials["password"]
        + "@"
        + database_credentials["ip"]
        + ":"
        + database_credentials["port"]
        + "/"
        + database_credentials["name"]
    )
except:
    connection_uri = os.environ["DB_URL"]

app.config["SQLALCHEMY_DATABASE_URI"] = connection_uri

db = SQLAlchemy(app)


city_fields = {
    "name": (City.name, str),
    "state": (City.state, str),
    "population": (City.population, int),
    "average_population_age": (City.average_population_age, int),
    "median_property_value": (City.median_property_value, int),
}

team_fields = {
    "name": (Team.name, str),
    "games_played": (Team.games_played, int),
    "runs": (Team.runs, int),
    "home_runs": (Team.home_runs, int),
    "hits": (Team.hits, int),
    "stolen_bases": (Team.stolen_bases, int),
    "wins": (Team.wins, int)
}

player_fields = {
    "name": (Player.name, str),
    "team.name": (Player.team_name, str),
    "age": (Player.age, int),
    "weight": (Player.weight, int),
    "height": (Player.height, str),
}

city_search = [
    City.name,
    City.state,
    City.largest_industry,
]

team_search = [
    Team.name,
    Team.short_name,
    Team.abbreviation,
    Team.city_name,
]

player_search = [
    Player.name,
    Player.city_name,
    Player.team_name,
    Player.birth_city,
    Player.position,
    Player.batting_side,
    Player.pitching_hand,
]


def get_filter(filter_by, filter_value):
    if filter_value == "":
        return None, False

    filter_value_range = filter_value.split("-")

    if len(filter_value_range) == 2:
        if filter_value_range[0] != "" and filter_value_range[1] != "":
            return (
                and_(
                    filter_by[0] >= filter_by[1](filter_value_range[0]),
                    filter_by[0] <= filter_by[1](filter_value_range[1]),
                ),
                True,
            )
        elif filter_value_range[0] == "" and filter_value_range[1] != "":
            return filter_by[0] <= filter_by[1](filter_value_range[1]), True
        elif filter_value_range[0] != "" and filter_value_range[1] == "":
            return filter_by[0] >= filter_by[1](filter_value_range[0]), True
        else:
            return None, False

    else:
        return filter_by[0] == filter_value, True


def get_filter_list(filter_by, filter_values):
    filter_value_list = filter_values.split(",")
    filter_query = None
    found_filter = False

    for filter_value in filter_value_list:
        additional_filter_query, found_additional_filter = get_filter(
            filter_by, filter_value
        )

        if found_additional_filter:
            if found_filter:
                filter_query = or_(filter_query, additional_filter_query)
            else:
                filter_query = additional_filter_query
                found_filter = True

    return filter_query, found_filter


def get_parameter(param):
    if param in request.args:
        return request.args.get(param)
    else:
        return ""


def parse_int(string, default_value):
    try:
        return int(string)
    except ValueError:
        return default_value


def get_search_query(search_for, search_fields):
    search_query = None

    found_search = False

    for search_field in search_fields:
        if found_search:
            search_query = or_(search_query, search_field.ilike("%" + search_for + "%"))
        else:
            found_search = True
            search_query = search_field.ilike("%" + search_for + "%")

    return search_query


def get_model_list(field_dict, search_fields, model):
    page = parse_int(get_parameter("page"), 1) - 1
    page_size = parse_int(get_parameter("pageSize"), 10)

    sort_by = get_parameter("sortBy")
    sort_dir = get_parameter("sortDir")

    filter_by_list = get_parameter("filterBy").split("|")
    filter_values_list = get_parameter("filterValues").split("|")

    search_for = get_parameter("searchFor")

    if sort_by in field_dict:
        sort_by = field_dict[sort_by][0]
        if sort_dir == "desc":
            sort_by = sort_by.desc()
    else:
        sort_by = None

    filter_query = None
    found_filter = False

    for (filter_by, filter_values) in zip(filter_by_list, filter_values_list):
        if filter_by in field_dict:
            additional_filter_query, found_additional_filter = get_filter_list(
                field_dict[filter_by], filter_values
            )

            if found_additional_filter:
                if found_filter:
                    filter_query = and_(filter_query, additional_filter_query)
                else:
                    filter_query = additional_filter_query
                    found_filter = True

    base_query = db.session.query(model).order_by(sort_by)

    if found_filter:
        base_query = base_query.filter(filter_query)

    if search_for != "":
        base_query = base_query.filter(get_search_query(search_for, search_fields))

    total_num = base_query.count()
    items = base_query.limit(page_size).offset(page * page_size).all()

    return (items, total_num)


@app.route("/")
def hello_world():
    return '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg" alt="cowboy" />'


@app.route("/city")
def city():
    cities, total_num = get_model_list(city_fields, city_search, City)

    response = jsonify(
        {"cities": [c.serialize() for c in cities], "totalNum": total_num}
    )
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/city/id=<id>")
def city_id(id):
    city = db.session.query(City).get(id)
    response = jsonify(city.serialize())
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/team")
def team():
    teams, total_num = get_model_list(team_fields, team_search, Team)

    response = jsonify({"teams": [t.serialize() for t in teams], "totalNum": total_num})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/team/id=<id>")
def team_id(id):
    team = db.session.query(Team).get(id)
    response = jsonify(team.serialize())
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/player")
def player():
    players, total_num = get_model_list(player_fields, player_search, Player)

    response = jsonify(
        {"players": [p.serialize() for p in players], "totalNum": total_num}
    )
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/player/id=<id>")
def player_id(id):
    player = db.session.query(Player).get(id)
    response = jsonify(player.serialize())
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
