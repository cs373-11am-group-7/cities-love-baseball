# 11am Group 7: Cities Love Baseball

## Name, EID, and GitLab ID, of all members

| Name             | EID     | Gitlab ID         |
| ---------------- | ------- | ----------------- |
| Nathan Eisenberg | nme437  | nathaneisenberg95 |
| Srikar Ganti     | sg49799 | srikarsganti      |
| Michael Kavas    | msk2522 | MichaelKavas      |
| Colette Montminy | crm4772 | cmontminy         |
| Matthew Whorton  | mdw3423 | MatthewWhorton    |

## Git SHA:

- Phase 1: 73fb846068c2962a899114a132aeeb9beb3ec476
- Phase 2: d101bed5061cb6bb5cf20c0420a2522c0d9669ea
- Phase 3: 2be3a29e23b317fb289e24893f89d85f4d355d80
- Phase 4: 12d9c02ec7bd13e999dcd8b475cf133529487e34

## Project Leaders:

- Phase 1: Matthew
- Phase 2: Nathan
- Phase 3: Srikar
- Phase 4: Colette

## Link to GitLab pipelines:

- https://gitlab.com/cs373-11am-group-7/cities-love-baseball/-/pipelines

## Link to website:

- https://www.citieslovebaseball.me/

## Link to API:

- https://api.citieslovebaseball.me/

## Estimated completion time for each member (hours: int)

| Name             | Phase 1 Hours | Phase 2 Hours | Phase 3 Hours | Phase 4 Hours |
| ---------------- | ------------- | ------------- | ------------- | ------------- |
| Nathan Eisenberg | 15            | 20            | 15            | 5             |
| Srikar Ganti     | 15            | 24            | 15            | 5             |
| Michael Kavas    | 20            | 15            | 15            | 5             |
| Colette Montminy | 20            | 30            | 15            | 5             |
| Matthew Whorton  | 20            | 40            | 25            | 10            |

## Actual completion time for each member (hours: int)

| Name             | Phase 1 Hours | Phase 2 Hours | Phase 3 Hours | Phase 4 Hours |
| ---------------- | ------------- | ------------- | ------------- | ------------- |
| Nathan Eisenberg | 15            | 28            | 15            | 5             |
| Srikar Ganti     | 14            | 24            | 10            | 4             |
| Michael Kavas    | 14            | 20            | 10            | 4             |
| Colette Montminy | 14            | 32            | 16            | 4             |
| Matthew Whorton  | 17            | 44            | 27            | 11            |

## Comments

- Used https://gitlab.com/forbesye/fitsbits/ for help with Selenium tests and CI
